

//
//**************** only support for Magellan V3.0 *******************************
//
#include "Magellan.h"
#include "MagMQTT.h"
//#include <avr/wdt.h>
Magellan magel;  
MagMQTT MQTT;
char auth[]="no-key";   //Token Key you can get from magellan platform
unsigned char server[]="13.76.29.21";
unsigned char port[]="1883";
//unsigned char username[]="Sim.{imsi}";  
unsigned char password[]="19.93.23.2";
unsigned char clientID[]="ggee"; //อะไรก็ได้
String Str_token;
bool state = false;
//bool substate = false;

void callback(String &topic,String &payload)
{
  Serial.println("msg "+topic+":"+payload);
}



void setup() {
  Serial.begin(115200);
//  wdt_disable();
  
  magel.begin(auth);           //init Magellan LIB
  String token = magel.thingsregister(); 
  String imsi=magel.imsi;
  String userChar ="Sim."+imsi;
  Str_token =token;
  char authMQTT[50];
  char imsi4MQTT[50];

 
  
  userChar.toCharArray(imsi4MQTT,50);
  token.toCharArray(authMQTT, 50);

  if(Str_token != NULL)
  {
    Serial.println("TOKEN KEY :"+token);
    Serial.println("IMSI :"+imsi);
//    Serial.println(userChar);

    MQTT.server=server;
    MQTT.port=port;
    MQTT.username=imsi4MQTT;
    MQTT.password=password;
    MQTT.clientID=clientID;
    Serial.println("MQTT Setup!!");
    MQTT.begin(authMQTT);
    
  }
  MQTT.RegisCallback(callback);
  MQTT.subscribe("delta/get/"+Str_token+"/SW1");
  delay(500);
}

void loop() {
 if(state == false)
 { 
 MQTT.publish("report/update/sim/v1/"+Str_token+"/98.207.254.136","{\"SW1\":\"1\"}");
 
 state = true;
 }
  MQTT.Loop();

//  if(substate ==false)
//  {
//    MQTT.subscribe("config/get/sim/v1/"+Str_token+"/SW1");
//    substate =true;
//  }
  /*
   Example send random temperature and humidity to Magellan IoT platform
  */
  String Temperature=String(random(0,100));
  String Humidity=String(random(0,100));
  String topic="report/update/sim/v1/"+Str_token+"/98.207.254.136";
  String payload="{\"FRE\":"+Humidity+",\"GED\":\"69\",\"RAFT\":"+Temperature+"}";
  MQTT.publish(topic,payload);
//
  
  delay(4000);
}
