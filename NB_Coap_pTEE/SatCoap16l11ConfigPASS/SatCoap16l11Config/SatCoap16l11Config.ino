/*
  supported board and hardware competible

  Arduino UNO please use software serial on pin8=RX pin9=TX and pin4=RST
  Arduino MEGA please use software serial on pin48=RX pin46=TX and pin4=RST
  NUCLEO_L476RG please use Hardware serial on pin2=RX pin8=TX and pin4=RST

  Pre required install library
         - Altsoftserial

  Example for post random data to Magellan IoT Platform
  please use payload with json format e.g. {"Temperature":25.5,"Humidity":90.4}
  and {"Location"["13.001","100.44"]} for plot the location on MAP
            |        |
          latitude  longitude

  and {"Lamp":0} or {"Lamp":1"} for show the Lamp status on dashboard


************** only support for Magellan V3.0 *****************************
*/
#include "Magellan.h"
Magellan magel;
#define LED 13
char auth[] = "no-key";
String payload;
String gps;
String token;
unsigned long lastmills = 0;
String Humid;
String Temp;
String Control;
String batt;
String Lamp = "0";
int prevpos;
int positive = 0;

void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  magel.begin(auth);
  token = magel.thingsregister();
}

void loop() {
  ///////// Control ///////////
  if (Control == "1")
  {
    Serial.println(">>>  ON");
    digitalWrite(LED, HIGH);
    Lamp = "1";
  }
  else if (Control == "0")
  {
    Serial.println(">>>  OFF");
    digitalWrite(LED, LOW);
    Lamp = "0";
  }
  ////////////////////////////

  if ((positive <= prevpos) || (positive >= prevpos))
  {
    positive += 5;
    prevpos = positive;
    if (positive == 100)
    {
      positive = 0;
    }
  }
  if (millis() - lastmills > 3000)
  {
    lastmills = millis();
    Temp = String(random(25, 38));
    Humid = String(random(20, 120));
    //    batt = String(random(5, 100));

    char *latz[] = {"13.7824875", "13.7825331", "13.7814935", "13.781578", "13.7834131"};
    char *lngz[] = {"100.5467064", "100.5464198", "100.5451758", "100.5454167", "100.5461027"};
    String ran = String(random(0, 5));
    int ranInt = ran.toInt();
    gps = "\"" + String(latz[ranInt]) + "," + String(lngz[ranInt]) + "\"";
  }
  payload = "{\"Temp\":" + Temp + ",\"Humid\":" + Humid + ",\"Batt\":" + String(positive) + ",\"Lamp\":" + Lamp + ",\"gps\":" + gps + "}";
  //    payload = "{\"Temp\":" + Temp + ",\"Humid\":" + Humid + ",\"Batt\":" + batt + ",\"Lamp\":" + Lamp + ",\"gps\":\"13.7824875,100.5467064\"}";

  magel.report(payload, token);
  Control = magel.getDelta("Lamp", token);
  delay(500);
}
