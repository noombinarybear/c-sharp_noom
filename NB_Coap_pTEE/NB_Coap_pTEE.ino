/*
  supported board and hardware competible
  
  Arduino UNO please use software serial on pin8=RX pin9=TX and pin4=RST
  Arduino MEGA please use software serial on pin48=RX pin46=TX and pin4=RST 
  NUCLEO_L476RG please use Hardware serial on pin2=RX pin8=TX and pin4=RST
  
  Pre required install library
         - Altsoftserial 

  Example for post random data to Magellan IoT Platform
  please use payload with json format e.g. {"Temperature":25.5,"Humidity":90.4}
  and {"Location"😞"13.001","100.44"]} for plot the location on MAP
            |        |
          latitude  longitude 

  and {"Lamp":0} or {"Lamp":1"} for show the Lamp status on dashboard


************** only support for Magellan V3.0 *****************************
*/
//#include <avr/wdt.h>
#include "Magellan.h"
#define WLED 13
//#define YLED 10
Magellan magel;          
char auth[]="no-key";     //Token Key you can get from magellan platform
//char auth[]="b3932dd6-2430-45c2-b33b-b4f6bf129665";     //Token Key you can get from magellan platform
String payload;
String token;
unsigned long lastmills=0;
unsigned long count=0;
String Control1;
String Config;
bool token_st =false;
bool gettoken =false;
int dynamicdelay =10000;
int delayConfig;
int cnt;
void setup() {
  Serial.begin(9600);
  pinMode(WLED,OUTPUT);
  digitalWrite(WLED,LOW);
  magel.begin(auth);   
  token = "5a179538-70b9-4447-a138-f278212f46a5"; 
  Config = magel.getConfig("Delay",token);
  delayConfig = Config.toInt();
  Serial.println(">>>This is my token :"+token);
  Serial.println(">>>This is my Config :"+Config);
  Serial.println("Defalt Delay :"+String(dynamicdelay));
}

void loop() {    
      if(millis()-lastmills >5000)
      {
        lastmills =millis();
        
        String randomVal = String(random(1,35));
        String randomVal_2 = String(random(30,120));
        payload="{\"Temperature\":"+randomVal+",\"Humidity\":"+randomVal_2+"}";  
      }
      
      Serial.println("Value Control >> SW1:"+Control1);
      if(Control1 =="1")
      {
        Serial.println("LED ON");
        digitalWrite(WLED,!LOW);
      }
      else if(Control1 =="0")
      {
        Serial.println("LED OFF");
        digitalWrite(WLED,LOW);
      }
       Control1 = magel.getDelta("SW1",token);
       
       magel.report(payload,token);   
       if((delayConfig != dynamicdelay)&&(delayConfig >=0))
       {
         dynamicdelay = delayConfig;
         Serial.println("Set New Delay From config Value :"+String(dynamicdelay));
       }
      delay(dynamicdelay);
      Serial.println("Delay in Loop :"+String(dynamicdelay));
}
