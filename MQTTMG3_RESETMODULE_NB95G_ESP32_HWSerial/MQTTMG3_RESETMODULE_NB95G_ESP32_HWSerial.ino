//##POC ESP32 Hardware Serial
#include <Arduino.h>

#define Rxpin 16   //RX2
#define Txpin 17   //TX2

String data_input;
String imsi;
unsigned int prev_mills;
String MQserver = "13.76.29.21";
String MQport = "1883";

String MQpassword = "10.0.0.1";


void setup()
{
  Serial.begin(115200);
  Serial1.begin(9600, SERIAL_8N1, Rxpin, Txpin);
  Serial.println("Start NBIoT...");
  delay(2000);
  check_module_ready();
  reboot_module();
  setup_module();
  setupMQTT();
}
void loop()
{
  //  reboot_mudule();
  if (millis() - prev_mills > 10000)
  {
    
    PubSend("register/update/sim/v1/" + imsi + "/98.207.254.136", "");
    Serial.println("Pub");
    prev_mills = millis();
  }
}

void serial_flush()
{
  while (1) {
    if (Serial1.available())
    {
      data_input = Serial1.readStringUntil('\n');
    }
    else
    {
      break;
    }
  }
}

void sendCommand(String command)
{
  Serial1.print(command + "\n");
  // wait_resp();
  delay(1000);
}

void wait_resp()
{
  delay(1000);
  while (Serial1.available())
  {
    Serial1.read();
  }
}

void reboot_module()
{
  Serial.println(">>Rebooting Mudule...");
  sendCommand("AT+NRB");
  delay(1000);
  while (Serial1.available())
  {
    data_input = Serial1.readStringUntil('\n');
    if (data_input.indexOf(F("OK")) != -1)
    {
      Serial.println(data_input);
      Serial.println("Hey Get OK!");
      break;
    }
    else
    {
      if (data_input.indexOf(F("REBOOT")) != -1)
      {
        Serial.println(data_input);
      }
      else
      {
        Serial.println(F("."));
      }
    }
  }
  delay(5000);
}

void check_module_ready()
{
  sendCommand("AT");
  delay(100);
  sendCommand("AT");
  while (1)
  {
    if (Serial1.available())
    {
      data_input = Serial1.readStringUntil('\n');
      if (data_input.indexOf(F("OK")) != -1)
      {
        Serial.println(F("Module OK"));
        break;
      }
    }
    else
    {
      if (millis() - prev_mills > 5000)
      {
        Serial.println(F("Error to connect module..."));
        prev_mills = millis();
      }
      else
      {
        sendCommand("AT");
      }
    }
  }
}

void setup_module()
{
  data_input = "";
  Serial.println(">>Setup Module");
  sendCommand("AT+CMEE=1");
  delay(500);
  Serial.print(F("."));

  serial_flush();
  data_input = "";
  Serial.println(">>IMSI");
  sendCommand("AT+CIMI");
  while (1)
  {
    if (Serial1.available())
    {
      data_input = Serial1.readStringUntil('\n');
      Serial.println(data_input);
      imsi = data_input;
      if (data_input.indexOf('52003') != -1)
      {
        imsi = data_input;
        if (imsi.length() > 15)
        {
          imsi.remove(15, imsi.length());
        }
        break;
      }
      else
      {
        sendCommand("AT+CIMI");
      }
    }
  }
  Serial.println("My IMSI :" + imsi);

  sendCommand("AT+CFUN=1");
  delay(6000);
  Serial.print(".");
  sendCommand("AT+NCONFIG=AUTOCONNECT,true");
  delay(500);
  Serial.println("OK");
  serial_flush();
  data_input = "";
}

void setupMQTT()
{
  Serial.println(">>Disconnect Client");
  sendCommand("AT+QMTDISC=0");
  while (1)
  {
    if (Serial1.available())
    {
      data_input = Serial1.readStringUntil('\n');
      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1) {
        break;
      }
      if (data_input.indexOf(F("ERROR")) != -1) {
        break;
      }
    }
  }
  data_input = "";
  delay(10000);
  Serial.println(">>Setup MQTT version");
  sendCommand("AT+QMTCFG=\"version\",0,4");
  while (1)
  {
    if (Serial1.available())
    {
      data_input = Serial1.readStringUntil('\n');
      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1)
      {
        break;
      }
    }
  }
  data_input = "";
  delay(7000);
  Serial.println(">>Open MQTT Connection");
  Serial.println("AT+QMTOPEN=0,\"" + MQserver + "\"," + MQport);
  sendCommand("AT+QMTOPEN=0,\"" + MQserver + "\"," + MQport);
  while (1)
  {
    if (Serial1.available())
    {
      data_input = Serial1.readStringUntil('\n');
      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1)
      {
        break;
      }
    }
  }
  delay(8000);
  Serial.println(">>Create Things");
  data_input = "";
  String MQclientID = imsi;
  String MQusername = "Sim." + imsi;
  sendCommand("AT+QMTCONN=0,\"" + MQclientID + "\",\"" + MQusername + "\",\"" + MQpassword + "\"");
  Serial.println("AT+QMTCONN=0,\"" + MQclientID + "\",\"" + MQusername + "\",\"" + MQpassword + "\"");
  //  data_input = "";
  delay(500);
  while (1) {
    if (Serial1.available())
    {
      data_input = Serial1.readStringUntil('\n');
      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1)
      {
//        break;
      }
      if(data_input.indexOf(F("+QMTCONN: 0,0,0")) != -1)
      {
        break;
      }
    }
  }
  //  data_input = "";
}

void PubSend(String topic, String payload)
{

  Serial.println(">>Pubblish");
  Serial.print("AT+QMTPUB=0,0,0,0\"" + topic + "\"" + payload);
  Serial1.print("AT+QMTPUB=0,0,0,0\"" + topic + "\"" + payload);
  Serial.write(0x26);
  Serial1.write(0x26);
//  Serial1.write(26);
//  Serial.println();
  while (1) {
    if (Serial1.available())
    {
     
      data_input = Serial1.readStringUntil('\n');
      Serial.println(data_input);

      // data_input =""; //noom  ////
      if (data_input.indexOf(F("OK")) != -1)
      {

      }
      else
      {
        break;
      }

    }
  }
  data_input = "";
}

void submsg(String topic)
{
  Serial.println("AT+QMTSUB=0,1,\"" + topic + "\",0");
  sendCommand("AT+QMTSUB=0,1,\"" + topic + "\",0");
}

//  void RegisCallback(ClientCallback)
