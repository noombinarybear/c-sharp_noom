// This example uses an ESP32 Development Board
// to connect to shiftr.io.
//
// You can check on your device after a successful
// connection here: https://shiftr.io/try.
//
// by Joël Gähwiler
// https://github.com/256dpi/arduino-mqtt
#include <WiFi.h>
#include <MQTT.h>

const char ssid[] = "Chocoolo2.4G";
const char pass[] = "Virus@coth";

const char tokenKey[] = "2d4c112b-fc70-42c4-a222-5bac8d83922e";
const char ipMagellan[] ="13.76.29.21";

WiFiClient net;
MQTTClient client;

unsigned long lastMillis = 0;

void connect() {
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.print("\nconnecting...");
  while (!client.connect("EDPWWQ",tokenKey,tokenKey));
  {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");

//  client.subscribe("/hello");
  // client.unsubscribe("/hello");
}

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
}

void setup() {
  Serial.begin(115200);
  WiFi.begin("Orbi The Gentry","Virus@coth");
//  WiFi.begin(ssid,pass);
  client.begin(ipMagellan,net);
//  client.onMessage(messageReceived);
  
  connect();
}

void loop() {
  client.loop();
  delay(10);  // <- fixes some issues with WiFi stability

  if (!client.connected()) {
    connect();
  }

  // publish a message roughly every second.
  if (millis() - lastMillis > 5000) {
    lastMillis = millis();
    String leet =String(random(0,100));
    client.publish("report/update/non/v1/"+String(tokenKey),"{\"ESP32_Ooh\":\""+leet+"\"}");
  }
}
