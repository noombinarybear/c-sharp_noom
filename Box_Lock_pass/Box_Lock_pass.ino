/*
||  Simple Password Entry Using Matrix Keypad
||  4/5/2012 Updates Nathan Sobieck: Nathan@Sobisource.com
||
*/


//* is to validate password   
//# is to reset password attempt

/////////////////////////////////////////////////////////////////

#include <Password.h> //http://www.arduino.cc/playground/uploads/Code/Password.zip
#include <Keypad.h> //http://www.arduino.cc/playground/uploads/Code/Keypad.zip

//Password password = Password( "1234" );
String keepST;
char Passdefault[]="6969";
char Passx[]="0000";
int checkwrong =0;
int checkdigi =0;
int doorcheck = 4;   //3v3 D4
int doorState =0;  //สเตตัสอ่านจากSenserแม่เหล็ก
int STdoor =false;   //กลไกลล็อค
int Secure =0;
int Enlock =0;
//Password password = Password(Passx);
//Password password.set = Passx;
const byte ROWS = 4; // Four rows
const byte COLS = 4; //  columns
// Define the Keymap

char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

//byte rowPins[ROWS] = { 9,8,7,6 };// Connect keypad ROW0, ROW1, ROW2 and ROW3 to these Arduino pins.
//byte colPins[COLS] = { 5,4,3,2, };// Connect keypad COL0, COL1 and COL2 to these Arduino pins.
byte rowPins[ROWS] = {5, 18, 19, 21}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {26, 25, 33, 32};

// Create the Keypad
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );
void setup(){
  
  Serial.begin(115200);
  keypad.addEventListener(keypadEvent); //add an event listener for this keypad
  pinMode(doorcheck, INPUT_PULLUP);
  pinMode(2,OUTPUT); //RED
  pinMode(15,OUTPUT); //Green
  digitalWrite(2,LOW);
  digitalWrite(15,LOW);
  Serial.println("Ready");
  alarmWrong();

}

void loop()
{
  keypad.getKey();
   doorState = digitalRead(doorcheck);
   
//   Serial.println(doorState); 
   delay(500);    
   if(doorState == 1)
  {
    Serial.println("Open");
    delay(1000);
    STdoor = true;
  }
  if(doorState == 0)
  { 
    Serial.println("Close");
    delay(1000);
    
    while(STdoor==true)
    {
      Serial.println("Coundown");
      Serial.println("1");
      delay(1000);
      Serial.println("2");
      delay(1000);
      Serial.println("3");
      delay(1000);
      Serial.println("4");
      delay(1000);
      Serial.println("5");
      delay(1000);
      Serial.println("LOCK");
      STdoor = !true;
      break;
    }
  }
}
Password password = Password(Passx);
Password password2 = Password(Passdefault);

//take care of some special events
void keypadEvent(KeypadEvent eKey){
  
  switch (keypad.getState()){
    case PRESSED: 
  Serial.print(eKey);
  switch (eKey){
    case '*': Serial.print("\n"); checkPassword();Serial.print("\n"); password2.reset();  break;
    case '#': Serial.print("\n"); password.reset(); Serial.print("\n"); password2.reset(); break;
    case 'D': generateNewPass(); break;
//    case 'A': StateLCK = 1; break;
    
    default: password.append(eKey); password2.append(eKey);
     }
  }
}
void generateNewPass(){
      keepST =random(1000,9999);
      Passx[5]; 
      keepST.toCharArray(Passx,5);
      Serial.println("The Password have been Change >>>>>>>>>>: ");
      Serial.print("New Password : ");
      Serial.println(Passx);

}

void alarmWrong(){
    
        if (checkwrong == 0)
    {
       Serial.println("Please Enter The Password 4 Digi");
    }
      if (checkwrong == 3)
    {
      Serial.println("You Have 2 Time Enter the Password");
    }
          if (checkwrong == 4)
    {
      Serial.println("\"Warming\" You Have 1 Time Enter the Password!!");

       digitalWrite(15,LOW);
       digitalWrite(2,!LOW);
       delay(200);
        digitalWrite(2,LOW);
       delay(200);
        digitalWrite(2,!LOW);
       delay(200);
        digitalWrite(2,LOW);
       delay(200);
        digitalWrite(2,!LOW);
       delay(200);
       digitalWrite(2,LOW);
    }
  if (checkwrong == 5)
    {
      generateNewPass();
      checkwrong =0;
      Serial.println("Wrong 5 Time Password Reset!!");
    }
}

void checkPassword(){
  if (password.evaluate()||password2.evaluate()){
    Serial.println("------------Success------------");
    password.reset();
    password2.reset();
    digitalWrite(15,!LOW);
    digitalWrite(2,LOW);
    checkwrong =0;
    Lock();
    
  }
  else
  {
    Serial.print("Wrong: ");
    Serial.print(checkwrong+1);
    Serial.println(" Time");
      checkwrong++;
      password.reset();
      password2.reset();
      digitalWrite(2,!LOW);
      digitalWrite(15,LOW);
      delay(1000);
      digitalWrite(2,LOW);
  }
  alarmWrong();
}
void Lock(){
  Serial.println("<<<<<<<<< UN LOCK >>>>>>>>");
}




