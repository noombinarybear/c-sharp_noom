////NOOM 24/10/62
////NOOM 25/10/62  Fix fail open 0,x and  stat 0,x  if x =0 success
////NOOM 31/10/62  pairing Signal RSSI
////NOOM 23/11/62  Fix RSSI in Method RSSI
////NOOM 4/12/62 Fix RSSI again and
////NOOM 12/12/62 Fix Error to restart
////NOOM 18/12/62 in Lib Fix bug restart
#include "MagMQTT.h"



#if defined( __ARDUINO_X86__)
#define Serial_PORT Serial1
#elif defined(ARDUINO_NUCLEO_L476RG)
#define Serial_PORT Serial1
#elif defined(ARDUINO_AVR_UNO) || (ARDUINO_AVR_MEGA2560)
AltSoftSerial module_serial;
#else
AltSoftSerial module_serial;
#endif



MagMQTT::MagMQTT()
{
}

bool MagMQTT::beginMQTT(char *auth)
{
  // bool created=false;
  // #if defined( __ARDUINO_X86__)
  //     Serial1.begin(9600);
  //     _Serial = &Serial1;
  //     //Serial.println(F("PLEASE USE Hardware Serial"))
  // #elif defined(ARDUINO_NUCLEO_L476RG)
  //     Serial1.begin(9600);
  //     _Serial = &Serial1;
  //     //Serial.println(F("PLEASE USE PIN RX=48 & TX=46"))
  // #elif defined(ARDUINO_AVR_UNO)
  //     module_serial.begin(9600);
  //     _Serial = &module_serial;
  // #elif defined(ARDUINO_AVR_MEGA2560)
  //     module_serial.begin(9600);
  //     _Serial = &module_serial;
  //     Serial.println(F("PLEASE USE PIN RX=48 & TX=46"));
  // #else
  //     module_serial.begin(9600);
  //     _Serial = &module_serial;
  // #endif

  // check_module_ready();
  // reboot_module();
  // setup_module();
  setupMQTT();
}

bool MagMQTT::Setup_Module_IMSI()
{
  bool created = false;

#if defined( __ARDUINO_X86__)
  Serial1.begin(9600);
  _Serial = &Serial1;
  //Serial.println(F("PLEASE USE Hardware Serial"))
#elif defined(ARDUINO_NUCLEO_L476RG)
  Serial1.begin(9600);
  _Serial = &Serial1;
  //Serial.println(F("PLEASE USE PIN RX=48 & TX=46"))
#elif defined(ARDUINO_AVR_UNO)
  module_serial.begin(9600);
  _Serial = &module_serial;
#elif defined(ARDUINO_AVR_MEGA2560)
  module_serial.begin(9600);
  _Serial = &module_serial;
  Serial.println(F("PLEASE USE PIN RX=48 & TX=46"));
#else
  module_serial.begin(9600);
  _Serial = &module_serial;
#endif

  check_module_ready();
  reboot_module();
  setup_module();

}

void MagMQTT::reboot_module()
{

  Serial.println(F(">>Rebooting "));
  _Serial->println(F("AT+NRB"));
  delay(1000);

  while (1) {
    if (_Serial->available()) {
      data_input = _Serial->readStringUntil('\n');
      if (data_input.indexOf(F("OK")) != -1) {
        Serial.println(data_input);
        break;
      }
      else {
        if (data_input.indexOf(F("REBOOT")) != -1)
        {
          Serial.println(data_input);
        }
        else {
          Serial.print(F("."));
        }
      }
    }
  }
  delay(10000);

}
void MagMQTT::setup_module()
{
  data_input = "";
  Serial.println(F(">>Setting"));
  _Serial->println(F("AT+CMEE=1"));
  delay(500);
  Serial.print(F("."));

  ///  Noom
  _serial_flush();
  data_input = "";
  Serial.print(F(">>IMSI "));
  _Serial->println(F("AT+CIMI"));

  while (1) {

    if (_Serial->available()) {
      data_input = _Serial->readStringUntil('\n');
      Serial.println(data_input);
      imsi = data_input;

      if (data_input.indexOf('52003') != -1)
      {
        imsi = data_input;
        if (imsi.length() > 15)
        {
          imsi.remove(15, imsi.length());
        }
        break;
      }

      else {

        _Serial->println(F("AT+CIMI"));
      }
    }
  }
  Serial.println();
  // data_input ="";
  // noom
  //NEW ADD NOOM 24/10/62

  _Serial->println(F("AT+CFUN=1"));
  delay(6000);
  Serial.print(F("."));
  _Serial->println(F("AT+NCONFIG=AUTOCONNECT,true"));
  delay(500);
  Serial.println(F("OK"));

  delay(4000);
  Serial.print(F(">>RSSI "));
  Serial.print(rssi());
  Serial.println(F(" dBm"));

  _serial_flush();
  data_input = "";

  String Qrssi = rssi();
  int rssi_  = Qrssi.toInt();
  if (rssi_ >= -65 && rssi_ != 404)
  {
    Serial.println("Signal Quality : Excellent!");
  }
  else if (rssi_ >= -75 && rssi_ != 404)
  {
  }
  else if (rssi_ >= -85 && rssi_  != 404)
  {
    Serial.println("Signal Quality : Fair!");

  }
  else if (rssi_ >= -95 && rssi_  != 404)
  {
    Serial.println("Signal Quality : Poor!");

  }
  else if (rssi_ <= -95 && rssi_  != 404)
  {
    Serial.println("Signal Quality : Disconnect!");
  }
  else
  {
    Serial.println("Signal Quality : Can't Detect Signal Quality!!");
  }
  /////////////////////
}

////noom add/////

String MagMQTT:: rssi()
{
  delay(200);
  _serial_flush();
  int rssi = 0;
  String data_csq = "";
  data_input = "";
  _Serial->println(F("AT+CSQ"));
  delay(1000);
  while (1)
  {
    if (_Serial->available())
    {
      data_input = _Serial->readStringUntil('\n');
      //Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1)
      {
        break;
      }
      else
      {
        if (data_input.indexOf(F("+CSQ")) != -1)
        {
          int start_index = data_input.indexOf(F(":"));
          int stop_index  = data_input.indexOf(F(","));
          data_csq = data_input.substring(start_index + 1, stop_index);
          if (data_csq == "99")
          {
            data_csq = "404";
          }

          else
          {
            rssi = data_csq.toInt();
            rssi = (2 * rssi) - 113;
            data_csq = String(rssi);
          }

        }
      }
    }
  }
  return data_csq;
}


////////////////




void MagMQTT::check_module_ready()
{
  _Serial->println(F("AT"));
  delay(100);
  _Serial->println(F("AT"));
  while (1) {
    if (_Serial->available()) {
      data_input = _Serial->readStringUntil('\n');
      if (data_input.indexOf(F("OK")) != -1) {
        hw_connected = true;
        Serial.println(F("Module OK"));
        break;
      }
    }
    else {
      unsigned int current_check = millis();
      if (current_check - previous_check > 5000)
      {
        Serial.println(F("Error to connect NB Module try reset or check your hardware"));
        previous_check = current_check;
        hw_connected = false;
        //break;
      }
      else {
        _Serial->println(F("AT"));
        delay(100);
      }
    }
  }

  _serial_flush();
}
void MagMQTT::_serial_flush()
{
  while (1) {
    if (_Serial->available()) {
      data_input = _Serial->readStringUntil('\n');
    }
    else {
      break;
    }
  }
}

void MagMQTT::setupMQTT()
{
  //Disconnect Client
  //AT+QMTDISC=0<CR><LF>


  Serial.println(F(">>Disconnect Client"));
  _Serial->println(F("AT+QMTDISC=0"));
  while (1) {
    if (_Serial->available()) {
      data_input = _Serial->readStringUntil('\n');
      Serial.println(data_input);
      String datasub = data_input.substring(0, 2);
      // Serial.println("datasub :"+datasub);

      if (datasub.startsWith("OK"))
      {
        Serial.println("Client Disconnected!...");
      }

      else if (datasub.startsWith("+C"))
      {
        Serial.println("Client is not yet connect!");
      }

      if (data_input.indexOf(F("OK")) != -1) {
        break;
      }
      if (data_input.indexOf(F("ERROR")) != -1) {
        break;
      }
    }
  }
  delay(10000);
  //AT+QMTDISC=0<CR><LF>
  //AT+QMTCFG="version",0,4<CR><LF>

  Serial.println(F(">>Setup MQTT version"));
  _Serial->println(F("AT+QMTCFG=\"version\",0,4"));
  while (1) {
    if (_Serial->available()) {
      data_input = _Serial->readStringUntil('\n');

      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1) {
        break;
      }
    }
  }
  data_input = "";
  delay(10000);
  //AT+QMTOPEN=0,"128.199.189.72",19000<CR><LF>
  Serial.println(F(">>Open MQTT Connection"));
  _Serial->print(F("AT+QMTOPEN=0,"));
  _Serial->print(F("\""));
  printchar(server);
  _Serial->print(F("\""));
  _Serial->print(F(","));
  printchar(port);
  _Serial->println();
  //_Serial->println(F("AT+QMTOPEN=0,\"128.199.189.72\",19000"));
  while (1) {
    if (_Serial->available()) {
      data_input = _Serial->readStringUntil('\n');
      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1) {
        break;
      }
    }
  }
  delay(10000);
  //AT+QMTCONN=0,"Mythings"<CR><LF>
  Serial.println(F(">>Create Things"));
  _Serial->print(F("AT+QMTCONN=0,"));
  _Serial->print(F("\""));
  printchar(clientID);
  _Serial->print(F("\""));

  if (strlen(username) > 1)
  {
    _Serial->print(F(","));
    _Serial->print(F("\""));
    printchar(username);
    _Serial->print(F("\""));
    _Serial->print(F(","));
    _Serial->print(F("\""));
    printchar(password);
    _Serial->print(F("\""));
  }

  _Serial->println();
  //_Serial->println(F("AT+QMTCONN=0,\"Mythings\""));
  while (1) {
    if (_Serial->available()) {
      data_input = _Serial->readStringUntil('\n');
      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1) {
        // data_input =""; //NOOM
        break;
      }
    }


    if (data_input.startsWith("+QMTOPEN: 0,"))
    {
      int chk_error2 = data_input.indexOf(F(","));
      String get_error2 = data_input.substring(chk_error2 + 1);
      // Serial.println("This Error : "+get_error2+" erorindex : "+chk_error2);
      if (get_error2.startsWith("0"))
      {
        get_detect = "200";
        return get_detect;
      }
      else
      {
        Serial.println("Can't Connection Back to Reset!  OPEN");
        //          check_module_ready();
        //          reboot_module();
        //          setup_module();
        //
        //          setupMQTT();
        get_detect = "404";
        return get_detect;
      }
    }

    if (data_input.startsWith("+QMTSTAT: 0,"))
    {
      int chk_error = data_input.indexOf(F(","));
      String get_error = data_input.substring(chk_error + 1);
      // Serial.println("This Error : "+get_error+" erorindex : "+chk_error);
      if (get_error.startsWith("0"))
      {
        get_detect = "200";
        return get_detect;
      }
      else
      {
        Serial.println("Connection is closed Back to Reset! STAT");
        //          check_module_ready();
        //          reboot_module();
        //          setup_module();
        //
        //          setupMQTT();
        get_detect = "404";
        return get_detect;
      }
    }
  }

  if (data_input.startsWith("+QMTCONN: 0,"))
  {
    int chk_error2 = data_input.indexOf(F(","));
    String get_error2 = data_input.substring(chk_error2 + 3);
    Serial.println("This Error : " + get_error2 + " erorindex : " + chk_error2);
    if (get_error2.startsWith("0"))
    {
      get_detect = "200";
      return get_detect;
    }
    else
    {
      Serial.println("Can't Connection Back to Reset! CONN");
      //          check_module_ready();
      //          reboot_module();
      //          setup_module();
      //
      //          setupMQTT();
      get_detect = "404";
      return get_detect;
    }
  }
  delay(10000);
}

void MagMQTT::printchar(char *str)
{
  char *hstr;
  hstr = str;
  while (*hstr)
  {
    Serial.print(*hstr);
    _Serial->print(*hstr);

    hstr++;
  }
}

void MagMQTT::publish(String topic, String payload)
{
  //PUB Atmost one
  //AT+QMTPUB=0,0,0,0,"we"<CR><LF>
  Serial.println(F(">>publish"));
  Serial.print(F("AT+QMTPUB=0,0,0,0,\""));
  _Serial->print(F("AT+QMTPUB=0,0,0,0,\""));
  Serial.print(topic);
  _Serial->print(topic);
  Serial.println(F("\""));
  _Serial->println(F("\""));
  Serial.print(F(">>payload="));
  Serial.println(payload);
  _Serial->print(payload);
  Serial.println(F(">>END"));
  Serial.write(26);
  _Serial->write(26);
  while (1) {
    if (_Serial->available())
    {
      data_input = _Serial->readStringUntil('\n');
      Serial.println(data_input);
      
      // data_input =""; //noom	////
      if (data_input.indexOf(F("OK")) != -1)
      {

      }

      if (data_input.startsWith("+QMTPUB: 0,"))
      {
        
        int chk_error2 = data_input.indexOf(F(","));
        String get_error2 = data_input.substring(chk_error2 + 1);
        //         Serial.println("This Error : "+get_error2+" erorindex : "+chk_error2);
        if (get_error2.startsWith("0"))
        {
          Serial.println(">>>Publish Success!!");
          get_detect = "200";
          return get_detect;
        }
        else
        {
          get_detect = "404";
          return get_detect;
        }
      }
      
      if (data_input.indexOf(F("+CME")) != -1)
      {
        
       if (countCME > 3)
        {
          get_detect = "404";
          return get_detect;
          break;   
        }       
        Serial.println("countCME : "+String(countCME)); 
        data_input ="";
        countCME++;
      }
      
    }
    else
    {
      break;
    }
  }
  //PUB Atlest one
  //AT+QMTPUB=0,1,1,0,"we"<CR><LF>

  //PUB Exactly one
  //AT+QMTPUB=0,1,2,0,"we"<CR><LF>


  //PUB Retain
  //AT+QMTPUB=0,1,2,1,"we"<CR><LF>
}


void MagMQTT::subscribe(String topic)
{
  //SUB
  //AT+QMTSUB=0,1,"we",0<CR><LF>
  Serial.println(F(">>Subscribe"));
  Serial.print(F("AT+QMTSUB=0,1,\""));
  _Serial->print(F("AT+QMTSUB=0,1,\""));
  Serial.print(topic);
  _Serial->print(topic);
  Serial.println(F("\",0"));
  _Serial->println(F("\",0"));
  get_detect = "200";
  return get_detect;
}



int MagMQTT::RegisCallback(MQTTClientCallback MQTT_func)
{
  int r = -1;

  if (callback_p == NULL)
  {
    callback_p = MQTT_func;
    r = 0;
  }
  //else - already in use! Fail!

  return r;
}

void MagMQTT::Loop()
{
  // data_input ="";  /// noom
  String topic = "NULL";
  String payload = "NULL";

  if (_Serial->available())
  {
    char data = (char)_Serial->read();
    if (data == '\n' || data == '\r')
    {
      if (k > 1)
      {
        end = true;
        //Serial.print("data_input is ");
        //Serial.println(data_input);
        k = 0;
      }
      k++;
    }
    else
    {
      data_input += data;
    }
  }
  if (end == true)
  {
    Serial.print("data_input is ");
    Serial.println(data_input);
    Serial.println(">>END>>>");
    if (data_input.startsWith(""))
    {
      Serial.println(">>END>>>");
    }
    if (data_input.indexOf(F("+CME")) != -1)
      {
        
       if (countCME > 2)
        {
          get_detect = "404";
          return get_detect;
//          break;   
        }       
        Serial.println("countCME : "+String(countCME)); 
        data_input ="";
        countCME++;
      }

    
    //     int index1 = data_input.indexOf(F(","));
    //     if(index1!=-1)
    //     {
    //       int index2 = data_input.indexOf(F(","),index1+1);
    //       index1 = data_input.indexOf(F(","),index2+1);

    // String prev_buffer=data_input.substring(index2+1,index1);
    // topic=prev_buffer;

    // index2 = data_input.indexOf(F(","),index1+1);
    // prev_buffer=data_input.substring(index1+1,index2);
    // payload=prev_buffer;

    // Serial.println(topic);
    // Serial.println(payload);
    // //Serial.println(data_input.substring(2,3));
    //   	}


    if (data_input.startsWith("+QMTPUB"))
    {
      int textsize;
      textsize = data_input.length();
      String data_buffer = data_input.substring(14);
      // Serial.println(data_input);
      // Serial.println("Yeah! : "+data_buffer);
      int index_ = data_buffer.indexOf(F(","));
      if (index_ != -1)
      {
        int index_2 = data_buffer.indexOf(F(","), index_ + 1);
        index_ = data_buffer.indexOf(F(","), index_2 + 1);

        String prev_buffer_ = data_buffer.substring(index_2 + 1, index_);
        topic = prev_buffer_;

        index_2 = data_buffer.indexOf(F(","), index_ + 1);
        prev_buffer_ = data_buffer.substring(index_ + 1, index_2);
        payload = prev_buffer_;

        // Serial.println(topic);
        // Serial.println(payload);
      }
    }
    else if (data_input.startsWith("+QMTRECV"))
    {

      int index1 = data_input.indexOf(F(","));

      if (index1 != -1)
      {
        int index2 = data_input.indexOf(F(","), index1 + 1);
        index1 = data_input.indexOf(F(","), index2 + 1);

        String prev_buffer = data_input.substring(index2 + 1, index1);
        topic = prev_buffer;

        index2 = data_input.indexOf(F(","), index1 + 1);
        prev_buffer = data_input.substring(index1 + 1, index2);
        payload = prev_buffer;



           if (data_input.startsWith("+QMTPUB: 0,"))
      {
        
        int chk_error2 = data_input.indexOf(F(","));
        String get_error2 = data_input.substring(chk_error2 + 1);
        //         Serial.println("This Error : "+get_error2+" erorindex : "+chk_error2);
        if (get_error2.startsWith("0"))
        {
          Serial.println(">>>Publish Success!!");
          get_detect = "200";
          return get_detect;
        }
        else
        {
          get_detect = "404";
          return get_detect;
        }
      }
      
      if (data_input.indexOf(F("+CME")) != -1)
      {
        
       if (countCME > 3)
        {
          get_detect = "404";
          return get_detect;
//          break;   
        }       
        Serial.println("countCME : "+String(countCME)); 
        data_input ="";
        countCME++;
      }
        //Serial.println(topic);
        // Serial.println(payload);
        //Serial.println(data_input.substring(2,3));
      }
    }

    if (callback_p != NULL)
    {
      callback_p(topic, payload);
    }
    else {
      Serial.println("NULL");
    }
    data_input = "";
    end = false;
  }

}
