#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "WiFi.h"
#include "HTTPClient.h"
#include "base64.h"

//#include <ESP8266WiFi.h>
const char* ssid = "Orbi The Gentry";
const char* password = "Virus@coth";
String authUsername = "858468c9-6f4b-44ce-8de5-94b3901e12ae";
String authPassword = "858468c9-6f4b-44ce-8de5-94b3901e12ae";

void connect()
    {
       while (WiFi.status() != WL_CONNECTED) 
        {
          delay(1000);
          Serial.println("Connecting to WiFi..");
        }
     
        Serial.println("Connected to WiFi"); 
     }
void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    connect();
    ESP.restart();
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
   ///เพิ่มBeginตรงนี้>>>
//   pinMode(2,OUTPUT);
//   pinMode(13,OUTPUT);
}

void loop() {
  ArduinoOTA.handle();
        ///เพิ่ม loopตรงนี้>>>
            if ((WiFi.status() == WL_CONNECTED)) 
            { //Check the current connection status          
              HTTPClient http;           
              http.begin("http://52.163.218.116:26000/api/report/non/v1");          
              String auth = base64::encode(authUsername + ":" + authPassword);
              http.addHeader("Authorization", "Basic " + auth);
              http.addHeader("x-ais-UserName","ESP32test"); 
              http.addHeader("x-ais-OrderRef","201909161730000123");
              http.addHeader("x-ais-OrderDesc","get config");
          //    int httpCode = http.GET(); 
              int httpCode = http.GET(); 
           
           
              if (httpCode > 0) { //Check for the returning code
           
                  String payload = http.getString();
                  Serial.println(httpCode);
                  Serial.println(payload);
              }
           
              else {
                Serial.println("Error on HTTP request");
              }
           
              http.end();
            }
           
            delay(10000);
}
