#include "MagMQTT.h"
MagMQTT magel;  

char auth[]="your-key";     //Token Key you can get from magellan platform

//unsigned char username[]="Sim.520039400017475";
unsigned char server[]="13.76.29.21";
unsigned char port[]="1883";

unsigned char password[]="10.1.1.1";
unsigned char clientID[]="";
unsigned char clientID2[]="nbiot";
String global_payload;
String global_topic;
String IMSI_;
boolean token_recv =false;
unsigned long millis_=0;
unsigned long lastMillis =0;
int hour_=0;
int minute_=0;
String Token_Key;




void re_open_mqtt()  // reconnect after Register and New Publish topic
{
  char backup_user[50];
  String backup_user_imsi ="Sim."+IMSI_; 
  backup_user_imsi.toCharArray(backup_user,50) ;
  magel.server=server;
  magel.port=port;
  magel.username=backup_user;
  magel.password=password;
  magel.clientID=clientID2;
  
  magel.beginMQTT(auth);
  Subscribe_list();
}

void get_token_fromregister()
{
   if(token_recv == false)
    {
       if(millis()-millis_ >10000)
       {
        millis_ =millis();
        magel.publish("register/update/sim/v1/"+IMSI_+"/98.207.254.136","");
       }
     
    }
}

void Subscribe_list()
{
  magel.subscribe("delta/get/"+Token_Key+"/data");
  delay(10000);
}


void callback(String &topic,String &payload)
{
  if((topic != "NULL")&&(payload != "NULL")&&(topic != NULL)&&(payload != NULL) )
  {
    Serial.println("It a msg :"+topic+" : "+payload);
    global_topic = topic;
    global_payload = payload;
    topic="";
    payload="";
    ///////////Get Token Key Magellan V3 ////////////
    if((global_topic == "\"register/get/sim/v1/"+IMSI_+"\"" ) && (global_payload != NULL))
      {
        Serial.println("This My Token : "+global_payload);
        Token_Key = global_payload;
        delay(5000);
        Serial.println("Re Open connect MQTT..");
        re_open_mqtt();
        token_recv =true;
      }
     /////////////////////////////////////////////////
    if((global_topic == "\"delta/get/"+Token_Key+"/data\"" ) && (global_payload != NULL))
      {
        
      }
  }
}
void setup() {
  Serial.begin(9600);
  Serial.println("Setup NB IoT!!...");
  //#######  Call IMSI ########
  magel.Setup_Module_IMSI();
  IMSI_ = magel.imsi;
  String user_imsi ="Sim."+IMSI_;
  Serial.println("USERNAME : "+user_imsi);
  char username[50];
  user_imsi.toCharArray(username,50) ;
  //###########################
  
  magel.server=server;
  magel.port=port;
  magel.username=username;
  magel.password=password;
  magel.clientID=clientID;
  
  magel.beginMQTT(auth);           //init Magellan LIB Connect to MQTT Magellan V.3
  
  magel.RegisCallback(callback);
  

  //#########Subscribe for register token with IMsSI to Magellan V.3########
  magel.subscribe("register/get/sim/v1/"+IMSI_);
  delay(15000);
  //#########################################################   
}

void loop() {
  // put your main code here, to run repeatedly:
 magel.Loop();
 get_token_fromregister();
 
if(token_recv == true)
  {
   if(millis() - lastMillis > 5000)
    {
        lastMillis = millis();
        String randdom=String(random(0,100));
        minute_++;
        
        String topic="report/update/sim/v1/"+Token_Key+"/98.207.254.136";
        String payload="{\"random\":"+randdom+",\"Hour_\":"+String(hour_)+",\"Minute\":"+String(minute_)+"}";
        magel.publish(topic,payload);
    }
   if(minute_ >= 60)
    {
        minute_ =0;
        hour_++;
        String topic="report/update/sim/v1/"+Token_Key+"/98.207.254.136";
        String payload="{\"Hour_\":"+String(hour_)+"}";
        magel.publish(topic,payload);
     }
      
  }
}
