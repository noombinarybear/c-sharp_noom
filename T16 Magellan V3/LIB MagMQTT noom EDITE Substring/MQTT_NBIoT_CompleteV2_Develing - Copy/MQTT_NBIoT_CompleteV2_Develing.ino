///// PASS Regist with out reconnect
#include "MagMQTT.h"
#define WLED 13
#define YLED 12
MagMQTT magel;  

char auth[]="your-key";     //Token Key you can get from magellan platform

//unsigned char username[]="Sim.520039400017475";
unsigned char server[]="13.76.29.21";
unsigned char port[]="1883";

unsigned char password[]="19.93.23.2";
unsigned char clientID[]="alias";

////// Sub Control for Get Value ////
String Subdelta ="SW1";
/////////////////////////////

//unsigned char clientID2[]="";
String global_payload;
String global_topic;
String IMSI_;
boolean token_recv =false;
boolean subscrb =false;
unsigned long millis_=0;
unsigned long lastMillis =0;
unsigned long lastMillis2 =0;
String Lamp ="0";
int hour_=0;
int minute_=0;
String Token_Key;
char username[50];

//#define LED 13


void get_token_fromregister()
{
   if(token_recv == false)
    {
       if(millis()-millis_ >10000)
       {
        millis_ =millis();
        magel.publish("register/update/sim/v1/"+IMSI_+"/98.207.254.136","");
       }     
    }
}

void Subscribe_list()
{
  if(subscrb == false)
  {
    magel.subscribe("delta/get/"+Token_Key+"/"+Subdelta);
    delay(5000);
    subscrb =true;
  }
}


void callback(String &topic,String &payload)
{
 
  if((topic != "NULL")&&(payload != "NULL")&&(topic != NULL)&&(payload != NULL) )
  {
    Serial.println("Incoming :"+topic+" : "+payload);
    global_topic = topic;
    global_payload = payload;
    ///////////Get Token Key Magellan V3 ////////////
    if((global_topic == "\"register/get/sim/v1/"+IMSI_+"\"" ) && (global_payload != NULL))
      {
        Serial.println("This My Token : "+global_payload);
        Token_Key = global_payload;
        delay(5000);
//        Serial.println("Got Token!! Reconnecting MQTT...");
//        re_open_mqtt();
        token_recv =true;
      }
     /////////////////////////////////////////////////
    if((global_topic == "\"delta/get/"+Token_Key+"/"+Subdelta+"\"" ) && (global_payload != NULL))
      {
        if(global_payload =="1")
          {
            digitalWrite(WLED,HIGH);
            Lamp ="1";
          }
        else if(global_payload =="0")
          {
            digitalWrite(WLED,LOW);
             Lamp ="0";
          }
            
      }
  }
}
void setup() {
  Serial.begin(9600);
  Serial.println(">>>>>>>>>>>>>>>>> Setup NBIoT <<<<<<<<<<<<<<<<<");
  pinMode(WLED,OUTPUT);
  digitalWrite(WLED,LOW);
  //#######  Call IMSI ########
  magel.Setup_Module_IMSI();
  IMSI_ = magel.imsi;
  String user_imsi ="Sim."+IMSI_;
  Serial.println("Username for Register : "+user_imsi);
//  char username[50];
  user_imsi.toCharArray(username,50) ;
  //###########################

  
  magel.server=server;
  magel.port=port;
  magel.username=username;
  magel.password=password;
  magel.clientID=clientID;
  
  magel.beginMQTT(auth);           //init Magellan LIB Connect to MQTT Magellan V.3
  
  magel.RegisCallback(callback);
  

  //#########Subscribe for register token with IMsSI to Magellan V.3########
  Serial.println("Register SIM to Magellan V.3");
  magel.subscribe("register/get/sim/v1/"+IMSI_);
 
  delay(10000);
  
  //#########################################################   
}

void loop() {
  // put your main code here, to run repeatedly:
 magel.Loop();
 get_token_fromregister();
 
// delay(6000);
// Subscribe_list();
if((Token_Key != NULL) &&  (token_recv ==true))
  {
     
   if(millis() - lastMillis > 60000)
    {
        lastMillis = millis();
        String randdom=String(random(0,100));
        minute_++;
        String topic="report/update/sim/v1/"+Token_Key+"/98.207.254.136";
        String payload="{\"randomsa\":"+randdom+",\"Hour_\":"+String(hour_)+",\"Minute\":"+String(minute_)+",\"SW1\":\""+Lamp+"\"}";
        magel.publish(topic,payload);
    }
   if(minute_ >= 60)
    {
        minute_ =0;
        hour_++;
        String topic="report/update/sim/v1/"+Token_Key+"/98.207.254.136";
        String payload="{\"Hour_\":"+String(hour_)+"}";
        magel.publish(topic,payload);
     }
    if(subscrb == false)
      {
        delay(5000);
        Subscribe_list();
        delay(10000);
      }
//    Call_sub();
  }
}

void Call_sub()
{
      
   if(millis() - lastMillis2 > 5000)
    {
        lastMillis2 = millis();
    
        String topic="report/update/sim/v1/"+Token_Key+"/98.207.254.136";
        String payload="{\"call_sub\":\"0\"}";
        magel.publish(topic,payload);
    } 
}
