#include <Wire.h>
#include <Arduino.h>
#include "Magellan.h"
Magellan magel;    
long randNumber;       
char auth[]="04263b60-fae5-11e9-a06d-290cc5e841d6";     //Token Key you can get from magellan platform
String payload;
void setup() {
  Serial.begin(9600);
  magel.begin(auth);           //init Magellan LIB
//    payload="{\"DeviceId\":\""+String(auth)+"\",\"Sensors\":[{\"Name\":\"Lamp\",\"Data\":\""+String("0")+"\"}]}";
//  magel.post2(payload);
  pinMode(LED_BUILTIN, OUTPUT);
}
void loop() {
  randNumber = random(50);
  payload="{\"DeviceId\":\""+String(auth)+"\",\"Sensors\":[{\"Name\":\"NUM\",\"Data\":\""+String(randNumber)+"\"}]}";
  magel.report(payload,auth);  
  magel.get("Lamp",auth);
  if(magel.get("Lamp",auth)=="1"){
    digitalWrite(LED_BUILTIN, HIGH);
    }else if(magel.get("Lamp",auth)=="0"){
      digitalWrite(LED_BUILTIN, LOW);
      }
  delay(2000);                                   
}
