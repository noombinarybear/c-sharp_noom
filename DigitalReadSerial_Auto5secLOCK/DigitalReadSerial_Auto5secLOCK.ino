/*
  DigitalReadSerial

  Reads a digital input on pin 2, prints the result to the Serial Monitor

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/DigitalReadSerial
*/

// digital pin 2 has a pushbutton attached to it. Give it a name:
int pushButton = 4;
int F = false;
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(pushButton, INPUT_PULLUP);   // 1 เท่ากับปล่อย  0 ติด
//  pinMode(Button, INPUT_PULLUP);
} 

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  int buttonState = digitalRead(pushButton);
  if(buttonState == 1)
  {
    Serial.println("Open");
    delay(1000);
    F = true;
  }
  if(buttonState == 0)
  { 
    Serial.println("Close");
    delay(1000);
    
    while(F==true)
    {
     
      Serial.println("1");
      delay(1000);
      Serial.println("2");
      delay(1000);
      Serial.println("3");
      delay(1000);
      Serial.println("4");
      delay(1000);
      Serial.println("5");
      delay(1000);
      Serial.println("LOCK");
      F = !true;
      break;
    }
  }
  // print out the state of the button:
  
  Serial.println(buttonState);
  delay(400);        // delay in between reads for stability
}
