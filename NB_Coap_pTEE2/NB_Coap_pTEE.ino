/*
  supported board and hardware competible

  Arduino UNO please use software serial on pin8=RX pin9=TX and pin4=RST
  Arduino MEGA please use software serial on pin48=RX pin46=TX and pin4=RST
  NUCLEO_L476RG please use Hardware serial on pin2=RX pin8=TX and pin4=RST

  Pre required install library
         - Altsoftserial

  Example for post random data to Magellan IoT Platform
  please use payload with json format e.g. {"Temperature":25.5,"Humidity":90.4}
  and {"Location"😞"13.001","100.44"]} for plot the location on MAP
            |        |
          latitude  longitude

  and {"Lamp":0} or {"Lamp":1"} for show the Lamp status on dashboard


************** only support for Magellan V3.0 *****************************
*/
//#include <avr/wdt.h>
#include "Magellan.h"
#define WLED 13
Magellan magel;
char auth[] = "no-key";   //Token Key you can get from magellan platform    //Token Key you can get from magellan platform
String payload;
String token;
unsigned long lastmills = 0;
String Control1 = "0";
String Lamp = "0";
String OldLamp;
int senddata = 0;
void setup()
{
  Serial.begin(9600);
  pinMode(WLED, OUTPUT);
  digitalWrite(WLED, LOW);
  magel.begin(auth);
  token = magel.thingsregister();
}

void loop()
{
  //  if (senddata == 1)
  //  {

  if (Control1 == "1")
  {
    //      Serial.println("LED ON");
    digitalWrite(WLED, HIGH);
    Lamp = "1";
  }
  else if (Control1 == "0")
  {
    //      Serial.println("LED OFF");
    digitalWrite(WLED, LOW);
    Lamp = "0";
  }
  //  }

  //  if ((millis() - lastmills > 4000) || (OldLamp != Lamp ))
  if (millis() - lastmills > 4000)
  {
    lastmills = millis();
    String randomVal = String(random(1, 35));
    String randomVal_2 = String(random(30, 120));
    String battery = String(random(5, 100));
    //    Serial.println("test :"+String(lats));
    //    payload = "{\"Temp\":" + randomVal + ",\"Humi\":" + randomVal_2 + ",\"Lamp\":" + Lamp + ",\"GPS\":\"" + lats + "," + lngs + "\",\"Batt\":" + battery + "}";
    payload = "{\"Temperature\":" + randomVal + ",\"Humidity\":" + randomVal_2 + ",\"SW1\":" + Lamp + "}";
    //    OldLamp = Lamp;
    magel.report(payload, token);
    Control1 = magel.getDelta("SW1", token);
    //    senddata = 1;
  }
  delay(4000);
  //      Serial.println("Value Control >> SW1:"+Control1);

}
