/*
  supported board and hardware competible

  Arduino UNO please use software serial on pin8=RX pin9=TX and pin4=RST
  Arduino MEGA please use software serial on pin48=RX pin46=TX and pin4=RST
  NUCLEO_L476RG please use Hardware serial on pin2=RX pin8=TX and pin4=RST

  Pre required install library
         - Altsoftserial

  Example for post random data to Magellan IoT Platform
  please use payload with json format e.g. {"Temperature":25.5,"Humidity":90.4}
  and {"Location"["13.001","100.44"]} for plot the location on MAP
            |        |
          latitude  longitude

  and {"Lamp":0} or {"Lamp":1"} for show the Lamp status on dashboard


************** only support for Magellan V3.0 *****************************
*/
#include "Magellan.h"
Magellan magel;
#define LED 13
#define LED2 53
#define LED3 52

////////////////////////
#define ledB  45
#define ledC  43
#define ledD  41
#define ledE  39
#define ledF  37
#define ledA  35

#define COMMON_ANODE

////////////////////////
char auth[] = "no-key";
String payload;
String gps;
String token;
unsigned long lastmills = 0;
String Humid;
String Temp;
String Control;
String Control2;
String Control3;
String batt;
String Lamp = "0";
String Lamp2 = "0";
String Lamp3 = "0";
int prevpos;
int positive = 0;

void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  pinMode(LED2, OUTPUT);
  digitalWrite(LED2, LOW);
  pinMode(LED3, OUTPUT);
  digitalWrite(LED3, LOW);

  pinMode(ledB, OUTPUT);
  pinMode(ledC, OUTPUT);
  pinMode(ledD, OUTPUT);
  pinMode(ledE, OUTPUT);
  pinMode(ledF, OUTPUT);
  pinMode(ledA, OUTPUT);
  setValue(0, 0, 0, 0, 0, 0);

  magel.begin(auth);
  token = magel.thingsregister();
}

void loop() {
  Control = magel.getDelta("Lamp", token);
  Control2 = magel.getDelta("Lamp2", token);
  Control3 = magel.getDelta("Lamp3", token);
  ///////// Control ///////////
  if (Control == "1")
  {
    Serial.println(">>> Lamp1 ON");
    digitalWrite(LED, HIGH);
    Lamp = "1";
  }
  else if (Control == "0")
  {
    Serial.println(">>> Lamp1 OFF");
    digitalWrite(LED, LOW);
    Lamp = "0";
  }
  ////////////////////////////
  ///////// Control2 ///////////
  if (Control2 == "1")
  {
    Serial.println(">>> Lamp2 ON");
    digitalWrite(LED2, HIGH);
    Lamp2 = "1";
  }
  else if (Control2 == "0")
  {
    Serial.println(">>> Lamp2 OFF");
    digitalWrite(LED2, LOW);
    Lamp2 = "0";
  }
  ////////////////////////////
  ///////// Control3 ///////////
  if (Control3 == "1")
  {
    Serial.println(">>> Lamp2 ON");
    digitalWrite(LED3, HIGH);
    Lamp3 = "1";
  }
  else if (Control3 == "0")
  {
    Serial.println(">>> Lamp2 OFF");
    digitalWrite(LED3, LOW);
    Lamp3 = "0";
  }
  ////////////////////////////

  if ((positive <= prevpos) || (positive >= prevpos))
  {
    positive += 5;
    prevpos = positive;
    if (positive == 100)
    {
      positive = 0;
    }
  }


 


  if (millis() - lastmills > 3000)
  {
    lastmills = millis();
    Temp = String(random(25, 38));
    Humid = String(random(20, 120));
    //    batt = String(random(5, 100));

    char *latz[] = {"13.7824875", "13.7825331", "13.7814935", "13.781578", "13.7834131"};
    char *lngz[] = {"100.5467064", "100.5464198", "100.5451758", "100.5454167", "100.5461027"};
    String ran = String(random(0, 5));
    int ranInt = ran.toInt();
    gps = "\"" + String(latz[ranInt]) + "," + String(lngz[ranInt]) + "\"";
  }
  payload = "{\"Temp\":" + Temp + ",\"Humid\":" + Humid + ",\"Batt\":" + String(positive) + ",\"Lamp\":" + Lamp + ",\"Lamp2\":" + Lamp2 + ",\"Lamp3\":" + Lamp3 + ",\"gps\":" + gps + "}";
  //    payload = "{\"Temp\":" + Temp + ",\"Humid\":" + Humid + ",\"Batt\":" + batt + ",\"Lamp\":" + Lamp + ",\"gps\":\"13.7824875,100.5467064\"}";
  
  magel.report(payload, token);
 if (positive >= 90)
  {
    setValue(255, 255, 255, 255, 255, 255);
  }
  else if (positive == 80)
  {
    setValue(255, 255, 255, 255, 255, 0);
  }
  else if (positive == 60)
  {
    setValue(255, 255, 255, 255, 0, 0);
  }
  else if (positive == 40)
  {
    setValue(255, 255, 255, 0, 0, 0);
  }
  else if (positive == 20)
  {
    setValue(255, 255, 0, 0, 0, 0);
  }
  else if (positive == 0)
  {
    setValue(255, 0, 0, 0, 0, 0);
  }
  delay(500);
}

/////              A          B          C          D         E         F
void setValue(int posA, int pos20, int pos40, int pos60, int pos80, int pos100)
{
#ifdef COMMON_ANODE
  posA = 255 - posA;
  pos20 = 255 - pos20;
  pos40 = 255 - pos40;
  pos60 = 255 - pos60;
  pos80 = 255 - pos80;
  pos100 = 255 - pos100;
#endif
  analogWrite(ledA, posA);
  analogWrite(ledB, pos20);
  analogWrite(ledC, pos40);
  analogWrite(ledD, pos60);
  analogWrite(ledE, pos80);
  analogWrite(ledF, pos100);
}
