//ESP32 NB IoT
#include <Arduino.h>
#include <CMMC_NB_IoT.h>
String data_input;
String imsi;
boolean flagReboot = false;
unsigned int prev_mills;
String MQserver = "13.76.29.21";
String MQport = "1883";
String MQclientID = imsi;
String MQpassword = "10.0.0.1";
String MQusername = "Sim."+imsi;
CMMC_NB_IoT nb;
CMMC_AT_Bridge &Bridge = nb.Bridge;

void setup()
{
  Serial.begin(9600);  //Serial
  Serial2.begin(9600); //NBIoT
  Bridge.init(&Serial, &Serial2);
  Serial.println("Start NBIoT...");
  check_mudule_ready();
  reboot_module();
  setup_module();
}

void loop()
{
  Bridge.loop();
  //  NBSerial.print("AT+NRB\n");
  //  reboot_module();
  setupMQTT();
}

void sendCommand(String command)
{
  Serial2.print(command + "\n");
  delay(1000);
}

void serial_flush()
{
  while (1) {
    if (Serial2.available())
    {
      data_input = Serial2.readStringUntil('\n');
    }
    else
    {
      break;
    }
  }
}

void check_mudule_ready()
{
  sendCommand("AT");
  delay(100);
  sendCommand("AT");
  while (1)
  {
    if (Serial2.available())
    {
      data_input = Serial2.readStringUntil('\n');
      if (data_input.indexOf(F("OK")) != -1)
      {
        Serial.println(F("Module OK"));
        break;
      }
    }
    else
    {
      if (millis() - prev_mills > 5000)
      {
        Serial.println(F("Error to connect module..."));
        prev_mills = millis();
      }
      else
      {
        sendCommand("AT");
      }
    }
  }
}

void reboot_module()
{
  sendCommand("AT+NRB");
  while (1)
  {
    if (Serial2.available())
    {
      data_input = Serial2.readStringUntil('\n');
      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1)
      {
        Serial.println(data_input);
        Serial.println("HEY");
        break;
      }
      else
      {
        if (data_input.indexOf(F("REBOOT")) != -1)
        {
          Serial.println(data_input);
          //         Serial.println("HEY2");
        }
        else
        {
          Serial.println(F("."));
        }
      }
    }
    data_input = "";
    delay(3000);
  }
}

void setup_module()
{
  data_input = "";
  Serial.println(">>Setup Module");
  sendCommand("AT+CMEE=1");
  Serial.print(F("."));

  serial_flush();
  data_input = "";
  Serial.println(">>IMSI");
  sendCommand("AT+CIMI");
  while (1)
  {
    if (Serial2.available())
    {
      data_input = Serial2.readStringUntil('\n');
      Serial.println(data_input);
      imsi = data_input;
      if (data_input.indexOf('52003') != -1)
      {
        imsi = data_input;
        if (imsi.length() > 15)
        {
          imsi.remove(15, imsi.length());
        }
        break;
      }
      else
      {
        sendCommand("AT+CIMI");
      }
    }
  }
  Serial.println("My IMSI :" + imsi);
  data_input = "";
  sendCommand("AT+CFUN=1");
  delay(6000);
  Serial.print(".");
  sendCommand("AT+NCONFIG=AUTOCONNECT,true");
  delay(500);
  Serial.println("OK");
  serial_flush();
}

void setupMQTT()
{
  Serial.println(">>Disconnect Client");
  sendCommand("AT+QMTDISC=0");
  while (1)
  {
    if (Serial2.available())
    {
      data_input = Serial2.readStringUntil('\n');
      Serial.println(data_input);
      String datasub = data_input.substring(0, 2);
      if (datasub.indexOf(F("OK")))
    {
      Serial.println("Client Disconnected!...");
      }
      else if (datasub.indexOf(F("+C")))
    {
      Serial.println("Client is not yet connect!...");
      }
      if (data_input.indexOf(F("OK")) != -1)
    {
      break;
    }
    else if (data_input.indexOf(F("ERROR")) != -1)
    {
      break;
    }
  }
}
data_input = "";
delay(10000);
  Serial.println(">>Setup MQTT version");
  sendCommand("AT+QMTCFG=\"version\",0,4");
  while (1)
  {
    if (Serial2.available())
    {
      data_input = Serial2.readStringUntil('\n');
      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1)
      {
        break;
      }
    }
  }
  data_input = "";
  delay(10000);
  Serial.println(">>Open MQTT Connection");
  sendCommand("AT+QMTOPEN=0,\"" + MQserver + "\"," + MQport);
  while (1)
  {
    if (Serial2.available())
    {
      data_input = Serial2.readStringUntil('\n');
      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1)
      {
        break;
      }
    }
  }
  delay(10000);
  Serial.println(">>Create Things");
  sendCommand("AT+QMTCONN=0,\"" + MQclientID + "\",\"" + MQusername + "\",\"" + MQpassword + "\"");
  data_input = "";
  while (1) {
    if (Serial2.available())
    {
      data_input = Serial2.readStringUntil('\n');
      Serial.println(data_input);
      if (data_input.indexOf(F("OK")) != -1) {
        break;
      }
    }
  }
}  
