/*
  supported board and hardware competible
  
  Arduino UNO please use software serial on pin8=RX pin9=TX and pin4=RST
  Arduino MEGA please use software serial on pin48=RX pin46=TX and pin4=RST 
  NUCLEO_L476RG please use Hardware serial on pin2=RX pin8=TX and pin4=RST
  
  Pre required install library
         - Altsoftserial 

  Example for post random data to Magellan IoT Platform
  please use payload with json format e.g. {"Temperature":25.5,"Humidity":90.4}
  and {"Location"😞"13.001","100.44"]} for plot the location on MAP
            |        |
          latitude  longitude 

  and {"Lamp":0} or {"Lamp":1"} for show the Lamp status on dashboard


************** only support for Magellan V3.0 *****************************
*/

#include "Magellan.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define OLED_RESET 4
Adafruit_SSD1306 display(-1);

Magellan magel;          
char auth[]="no-key";     //Token Key you can get from magellan platform
//char auth[]="874c6817-55e5-42b5-ab9a-39a48ae6583f";     //Token Key you can get from magellan platform
String payload;
String token;
unsigned long lastmills=0;
unsigned long count=0;
void setup() {
  Serial.begin(9600);
  magel.begin(auth);   
  token=magel.thingsregister();  
  display.begin(SSD1306_SWITCHCAPVCC, 0x3c); //initialize I2C addr 0x3c
  display.clearDisplay(); // clears the screen and buffer
         //init Magellan LIB
}

void loop() {

  /*
    Example send random temperature and humidity to Magellan IoT platform
  */
//  String Temperature=String(random(0,100));
//  String Humidity=String(random(0,100));
//  String Cnt=String(count);

//  payload="{\"Temperature\":"+Temperature+",\"Humidity\":"+String(count)+"}";
//  payload="{\"Temperature\":"+Temperature+",\"Humidity\":"+Humidity+",\"counter\":"+String(count)+"}";       //please provide payload with json format
  if(millis()-lastmills > 10000)
  {
    lastmills =millis();
    count++;
  }


  //////////// OLED///////////
  display.setCursor(0,0);
  display.setTextSize(1);
  display.println(count);
  display.setTextColor(WHITE);
  display.display();
  ///////////////////////////
  
  //magel.post(payload);
//  String token=magel.thingsregister();   
//  String token=auth;//post payload data to Magellan IoT platform
  Serial.print("token=");
  Serial.println(token);  
  Serial.println("CounterTimePerMinute =");
  Serial.println(count);
  
  magel.report(payload,token);

  magel.getConfig("zero",token);
  magel.getDelta("SW",token);
}
