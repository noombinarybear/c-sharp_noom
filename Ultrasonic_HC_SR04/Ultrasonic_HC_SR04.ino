#include <HCSR04.h>

UltraSonicDistanceSensor distanceSensor(12, 14);  // Initialize sensor that uses digital pins 13 and 12.

void setup () {
    Serial.begin(9600);  // We initialize serial connection so that we could print values from sensor.
}

void loop () {
    // Every 500 miliseconds, do a measurement using the sensor and print the distance in centimeters.
    Serial.print(distanceSensor.measureDistanceCm());
    Serial.println(" Cm");
    delay(500);
}
