/*
  supported board and hardware competible
  
  Arduino UNO please use software serial on pin8=RX pin9=TX and pin4=RST
  Arduino MEGA please use software serial on pin48=RX pin46=TX and pin4=RST 
  NUCLEO_L476RG please use Hardware serial on pin2=RX pin8=TX and pin4=RST
  
  Pre required install library
         - Altsoftserial 

  Example for post random data to Magellan IoT Platform
  please use payload with json format e.g. {"Temperature":25.5,"Humidity":90.4}
  and {"Location"😞"13.001","100.44"]} for plot the location on MAP
            |        |
          latitude  longitude 

  and {"Lamp":0} or {"Lamp":1"} for show the Lamp status on dashboard


************** only support for Magellan V3.0 *****************************/

#include "Magellan.h"
Magellan magel;

#define led_w 12
char auth[]="no-key";     //Token Key you can get from magellan platform
String payload;
String Token_Key;
//unsigned long lastmills=0;
//unsigned long count=0;
String RCVDelay;
String StateRCV;
String Control1;
int config_delay =10000; //10Sec
char tokenChar[50];
int cnt;

void setup() {
  Serial.begin(9600);
  pinMode(led_w,OUTPUT);
  digitalWrite(led_w,LOW);
  magel.begin(auth); 
  
  Token_Key=magel.thingsregister(); 
  Serial.println("Token :"+Token_Key);
  Token_Key.toCharArray(tokenChar,50);
  cnt =(strlen(tokenChar));
 
  

}

void loop() { 

      
       Serial.println(">>>Thismy token :"+Token_Key);
       payload="{\"Temperature\":\"69\",\"Humidity\":\"55\"}"; 
//       Control1 = magel.getDelta("SW1",Token_Key); 
       magel.report(payload,Token_Key); 
              
       Serial.println("Value Control >> random :"+Control1);
       delay(5000);
  
//  payload="{\"Temperature\":\"66\",\"Humidity\":\"69\"}";       //please provide payload with json format
    
//   Serial.println("Counter Token :"+String(cnt));
//   RCVDelay = magel.getConfig("Delay",Token_Key);
//   if(cnt > 0)
//   { 
//   Serial.println("Have Token Key !!!"); 
//   int delayInt = RCVDelay.toInt();
//   Serial.println("Delay Value RSP :"+String(delayInt));
//       if(delayInt != config_delay)
//        {
//          config_delay = delayInt;
//          Serial.print("Set New Config delay!! :");
//          Serial.print(config_delay);
//          Serial.println(" milli Sec.");
//        }
//   }
//   delay(config_delay);
//   Serial.println("Delay in Loop :"+String(config_delay));
//   
}
