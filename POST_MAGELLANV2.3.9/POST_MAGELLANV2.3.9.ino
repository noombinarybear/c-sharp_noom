//สำหรับ Magellan ธรรมดา V2.3.9
//ส่งได้แต่ไม่เปลี่ยนค่าอีกเลยส่งไม่หยุดเลย สามารถส่งไปยัง ifarm ได้
#include "Magellan2_39.h"
//#include "Magellan1_5.h"
Magellan magel;
char auth[]="5320f110-be36-11e9-bef6-2fcacadc431c"; 		//Token Key you can get from magellan platform
String Temperature;
String Humidity;
String payload;

void setup() {
  Serial.begin(9600);
  magel.begin(auth);           //init Magellan LIB
}

void loop() {

  /*
  	Example send random temperature and humidity to Magellan IoT platform
  */
  Temperature=String(random(0,100));
  Humidity=String(random(0,100));

//  payload="{\"Temperature\":"+Temperature+",\"Humidity\":"+Humidity+"}";       //please provide payload with json format
//payload = "{\"DeviceId\":\"5320f110-be36-11e9-bef6-2fcacadc431c\",\"Sensors\":[{\"Name\":\"Temp\",\"Data\":\"30\"},{\"Name\":\"Hum\",\"Data\":\"60\"}]}";
payload = "{\"DeviceId\":\"5320f110-be36-11e9-bef6-2fcacadc431c\",\"Sensors\":[{\"Name\":\"Temp\",\"Data\":"+Temperature+"},{\"Name\":\"Hum\",\"Data\":"+Humidity+"}]}"; /// For iFarm Coap
delay(4000);
  magel.post(payload);                            							   //post payload data to Magellan IoT platform
  Serial.println(payload);
  Serial.println(Temperature+Humidity);
//  delay(4000);
}
