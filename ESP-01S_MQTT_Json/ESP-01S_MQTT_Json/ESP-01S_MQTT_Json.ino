#include "ESP8266WiFi.h"
#include <MQTT.h>
#include <ArduinoJson.h>
#define RELAY 0
#define LED 2

const char* ssid = "Orbi The Gentry"; //กำหนด SSID 
const char* password = "Virus@coth"; //กำหนด Password
const char* keyToken ="c43ebf80-df38-11e9-a06d-290cc5e841d6";
//const char* ipMagellan ="13.67.108.80"; // ifarm
const char* ipMagellan ="13.67.108.80"; // ifarm
int port =1883;
String Tp;
String Pl;
int SW; //Status Switch
unsigned char status_RELAY = 0;
unsigned long lastMillis=0;

WiFiClient net;
MQTTClient client(1024);
StaticJsonDocument<200> doc;
void connect()
{
  Serial.print("Checking wifi...");
  while (WiFi.status() != WL_CONNECTED)
    {
      Serial.print(".");
      delay(1000);
    }
  Serial.print("\nconnecting...");
  while (!client.connect("TRW","admin","admin"))
   {
    Serial.print(".");
    delay(1000);
   }
   Serial.print("\nconnected!!");
   client.subscribe("dashboards/data/"+String(keyToken));
   client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Value\",\"Data\":\"34\"\"},{\"Name\":\"Input2\",\"Data\":\"1\"},{\"Name\":\"SwitchESP-01S\",\"Data\":\"1\"}]}");
}   

void messageReceived(String &topic, String &payload)
          {
             Serial.println("incoming: " + topic + " - " + payload);
             //### TP =topic  PL=payload###
               Tp =topic;
               Pl =payload;
              if(Tp =="dashboards/data/" + String(keyToken))
                {
                 DeserializationError error = deserializeJson(doc, Pl);
                }

             SW = doc["SwitchESP-01S"];
             if(SW == 1)
             {
                digitalWrite(RELAY,HIGH);
                Serial.println("Relay ON!");  
                client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Input2\",\"Data\":\"1\"},{\"Name\":\"SwitchESP-01S\",\"Data\":\"1\"}]}");
                
             }
            
            if(SW == 0)
             {
                digitalWrite(RELAY,!HIGH);
                Serial.println("Relay OFF!");
                client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Input2\",\"Data\":\"0\"},{\"Name\":\"SwitchESP-01S\",\"Data\":\"0\"}]}");
             }
            
          }

void setup() {
Serial.begin(115200);
pinMode(RELAY, OUTPUT);
pinMode(LED, OUTPUT);
digitalWrite(LED, HIGH);
Serial.println();
Serial.print("Connecting to ");
Serial.println(ssid);
WiFi.begin(ssid, password);
while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
        digitalWrite(LED,HIGH);
        delay(400);   
        digitalWrite(LED,LOW);        
    }
Serial.println("");
Serial.println("WiFi connected");
Serial.println("Server started");
Serial.println(WiFi.localIP()); // แสดงเลข IP ของ ESP8266
client.begin(ipMagellan, port, net);
Serial.println("MQTT Connect!");
client.onMessage(messageReceived);
digitalWrite(LED, LOW);

connect();
}

void loop() 
{
    client.loop();
    delay(10);  
     if (!client.connected()) 
     {
       connect();
     }

    if(millis() - lastMillis > 10000) 
    {
        lastMillis = millis();
       String randdit = String(random(0,200));
          ///
//       client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Input2\",\"Data\":\"1\"},{\"Name\":\"SwitchESP-01S\",\"Data\":\"1\"}]}");
          ///
       client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Random\",\"Data\":\""+randdit+"\"}]}");
       
       Serial.println("Published   "+randdit);
    }

}
