// This example uses an Adafruit Huzzah ESP8266
// to connect to shiftr.io.
//
// You can check on your device after a successful
// connection here: https://shiftr.io/try.
//
// by Joël Gähwiler
// https://github.com/256dpi/arduino-mqtt

#include <ESP8266WiFi.h>
#include <MQTT.h>
#include "DHT.h"
#include <WiFiManager.h>
////// Ssid&Pass AP MODE //////
#define ESP_AP_NAME "SET_ME_SSID"
#define ESP_AP_PASS ""

#define DHTPIN D5  
#define DHTTYPE DHT22 
#define LED D6
#define Switch D7  //SW ON OFF
#define SWRST D8  //SW Reset
int State;    //Status LED
int ST_SW;    //Status SW ON OFF
int ST_RST;  //Status SW Reset
DHT dht(DHTPIN, DHTTYPE);

const char ssid[] = "Orbi The Gentry";
const char pass[] = "Virus@coth";

//const char ssid[] = "LittleNoom";
//const char pass[] = "ad22334455";


const char* keyToken ="692b84b0-ac58-11e9-9aef-8de86578e580";
const char* ipMagellan ="13.67.108.80";

WiFiManager wifiManager;
WiFiClient net;
MQTTClient client(1024);  //1024คือจำนวนไบต์ ไบต์น้อย ชื่อตรง ESLR ซ้ำจะทำให้โดนเด้งออกจากเซิฟเวอร์


unsigned long lastMillis = 0;

void connect() {
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.print("\nconnecting...");
  while (!client.connect("ESLR","admin","admin")) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");

//    client.subscribe("/hello");
    client.subscribe("dashboards/data/" +String(keyToken));
  // client.unsubscribe("/hello");
}

void ZWITCH()
      {
       ST_SW = digitalRead(Switch);
           if(ST_SW == LOW)
            {
              if(State ==0)
                 {
                  digitalWrite(LED,HIGH);
                  State =1;
                  client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"1"+"\"},{\"Name\":\"STATE\",\"Data\":\"1"+"\"}]}"); 
                  Serial.print("STATE =");
                  Serial.println(State);
                  delay(200);
                 }
                 else
                 {
                  digitalWrite(LED,LOW);
                  State =0;
                  client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"0"+"\"},{\"Name\":\"STATE\",\"Data\":\"0"+"\"}]}"); 
                  Serial.print("STATE =");
                  Serial.println(State);
                  delay(200);
                
                 }
            }  

      }
  
void messageReceived(String &topic, String &payload) 
          {
             Serial.println("incoming: " + topic + " - " + payload);
          
             if(topic =="dashboards/data/" + String(keyToken) && payload =="{\"SW\":0}")
              {
                digitalWrite(LED,LOW);
                Serial.println("OFF");
                 State = 0; 
 
//                 client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"0"+"\"},{\"Name\":\"STATE\",\"Data\":\""+String(State)+""+"\"}]}");
              }
             if(topic =="dashboards/data/" + String(keyToken) && payload =="{\"SW\":1}")
              {
                digitalWrite(LED,HIGH);
                Serial.println("ON");
                 State = 1;  
//                 client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"1"+"\"},{\"Name\":\"STATE\",\"Data\":\""+String(State)+""+"\"}]}"); 
              }
                           
             
//            if(ST_SW == HIGH)
//            {
//               digitalWrite(LED,LOW);
////             Serial.println("OFF");
//               State = 0; 
//               client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"0"+"\"},{\"Name\":\"STATE\",\"Data\":\""+String(State)+""+""+"\"}]}"); 
//               
//            }
       
          }
             

void setup() {
  pinMode(SWRST,INPUT_PULLUP);
  pinMode(Switch,INPUT_PULLUP);
  pinMode(LED,OUTPUT);
  Serial.begin(115200);
  dht.begin();
  WiFi.begin(ssid, pass);
 
  // Note: Local domain names (e.g. "Computer.local" on OSX) are not supported by Arduino.
  // You need to set the IP address directly.
  client.begin("13.67.108.80", net);

  client.onMessage(messageReceived);

  connect();
}

void loop() {
  client.loop();
  delay(10);  // <- fixes some issues with WiFi stability

  if (!client.connected()) {
    connect();
  }

  ZWITCH();
          
  
  // publish a message roughly every second.
  if (millis() - lastMillis > 3000) {
    lastMillis = millis();
      int a = random(100,200);  //random(ค่า); //random(ค่าจาก,ค่าถึง);
      float h = dht.readHumidity();
      // Read temperature as Celsius (the default)
      float t = dht.readTemperature();
      // Read temperature as Fahrenheit (isFahrenheit = true)
      float f = dht.readTemperature(true);
//    client.publish("/hello", "world");
//    client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Value\",\"Data\":\""+String(a)+""+"\"}]}"); 
     //Serial.print("Temp=");
    // Serial.println(t);
 //    Serial.print("STATE =");
 //    Serial.println(State);
//     client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Temp\",\"Data\":\""+String(t)+""+"\"}]}"); 
//     client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"0"+"\"}]}"); 
//     client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"STATE\",\"Data\":\""+String(State)+""+"\"}]}"); 
     client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Value\",\"Data\":\""+String(a)+"\"},{\"Name\":\"Temp\",\"Data\":\""+String(t)+"\"},{\"Name\":\"SW\",\"Data\":\""+String(State)+""+"\"},{\"Name\":\"STATE\",\"Data\":\""+String(State)+""+"\"}]}"); 
  }
}
