#define red D4
#define green D5
#define blue D6
int RGBPin[] = { D4, D5, D6};

void setup() {
  pinMode(RGBPin[0], OUTPUT);
  pinMode(RGBPin[1], OUTPUT);
  pinMode(RGBPin[2], OUTPUT);
}

void SetRGB(int r, int g, int b) {
  analogWrite(RGBPin[0], r);
  analogWrite(RGBPin[1], g);
  analogWrite(RGBPin[2], b);
}

void loop() {
  SetRGB(255, 0, 0); // Red
  delay(500);
  SetRGB(0, 255, 0); // Green
  delay(500);
  SetRGB(0, 0, 255); // Blue
  delay(500);
  SetRGB(255, 255, 0); // Yellow
  delay(500);
  SetRGB(0, 255, 255); // Sky blue
  delay(500);
  SetRGB(255, 0, 255); // Pink
  delay(500);
  SetRGB(255, 255, 255); // White
  delay(500);
}
