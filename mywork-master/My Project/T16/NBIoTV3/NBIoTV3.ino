/*
  supported board and hardware competible
  
  Arduino UNO please use software serial on pin8=RX pin9=TX and pin4=RST
  Arduino MEGA please use software serial on pin48=RX pin46=TX and pin4=RST 
  NUCLEO_L476RG please use Hardware serial on pin2=RX pin8=TX and pin4=RST
  
  Pre required install library
         - Altsoftserial 

  Example for post random data to Magellan IoT Platform
  please use payload with json format e.g. {"Temperature":25.5,"Humidity":90.4}
  and {"Location"😞"13.001","100.44"]} for plot the location on MAP
            |        |
          latitude  longitude 

  and {"Lamp":0} or {"Lamp":1"} for show the Lamp status on dashboard


************** only support for Magellan V3.0 *****************************
*/

#include "Magellan.h"
Magellan magel;          
char auth[]="no-key";     //Token Key you can get from magellan platform
//char auth[]="b3932dd6-2430-45c2-b33b-b4f6bf129665";     //Token Key you can get from magellan platform
String payload;
String token;
unsigned long lastmills=0;
//unsigned long lastmills2=0;
unsigned long count=0;
//String Temperature;
//String Humidity;
void setup() {
  Serial.begin(9600);
  magel.begin(auth);   
  token=magel.thingsregister();  
         //init Magellan LIB
}

void loop() {

 
//  if(millis()-lastmills > 60000)
//  {
//    lastmills =millis();
//    count++;
//
//  }
//  String Cnt=String(count);
  String Temperature=String(random(0,100));
  String Humidity=String(random(0,100));
      delay(1000);
//  payload="{\"Temperature\":"+Temperature+",\"Humidity\":"+String(count)+"}";
  payload="{\"Temperature\":"+Temperature+",\"Humidity\":"+Humidity+"}";       //please provide payload with json format
    
   

//  magel.post(payload);
//  String token=magel.thingsregister();   
//  String token=auth;//post payload data to Magellan IoT platform
  Serial.print("token=");
  Serial.println(token);  
  Serial.println("CounterTimePerMinute =");
  Serial.println(count);
  
  magel.report(payload,token);

  magel.getConfig("zero",token);
  magel.getDelta("SW",token);
}
