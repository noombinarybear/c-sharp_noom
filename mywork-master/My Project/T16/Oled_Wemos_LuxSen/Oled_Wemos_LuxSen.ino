/*
  This example uses only 4 wires to test the BH1750 Light sensor
  
  Connecting the sensor to a NodeMCU ESP8266:
  VCC  <-> 3V3
  GND  <-> GND
  SDA  <-> D2
  SCL  <-> D1

  Connecting the sensor to a Arduino UNO:
  VCC  <-> 3V3
  GND  <-> GND
  SDA  <-> A4/SDA 
  SCL  <-> A5/SCL

  Connecting the sensor to a Arduino DUE:
  VCC  <-> 3V3
  GND  <-> GND
  SDA  <-> D20/SDA
  SCL  <-> D21/SCL
*/

#include <BH1750FVI.h>
#include <Wire.h> 
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
Adafruit_SSD1306 OLED(-1); 


// Create the Lightsensor instance
BH1750FVI LightSensor(BH1750FVI::k_DevModeContLowRes);
uint16_t lux =0;
uint16_t prev;
void setup() 
{
  Serial.begin(115200);
  LightSensor.begin();  
 OLED.begin(SSD1306_SWITCHCAPVCC,0x3C);
 OLED.clearDisplay();
 OLED.setTextColor(WHITE,BLACK); 
 OLED.setCursor(0,0);
 OLED.setTextSize(2); 
 OLED.println("Lux SenSor");
 OLED.println("Ready");
 OLED.display();
 OLED.clearDisplay();
 delay(2000);

}

void loop()
{
  prev =lux;
  
  lux = LightSensor.GetLightIntensity();
  
  if(prev >lux)
    {
      Serial.println("UP");
//      OLED.clearDisplay();
//      OLED.setTextSize(3);
      OLED.fillTriangle(5,20,11,13,17,20,WHITE);
//      OLED.drawTriangle(5,20,11,13,17,20,WHITE);
      OLED.display();
      delay(400);
    }
    else if(prev <lux)
    {
      Serial.println("Down");
//      OLED.clearDisplay();
//      OLED.setTextSize(3);
      OLED.fillTriangle(5,20,11,27,17,20,WHITE);
//      OLED.drawTriangle(5,20,11,27,17,20,WHITE);
      OLED.display();
      delay(400);
    }
  OLED.clearDisplay();
    OLED.setTextColor(WHITE,BLACK); 
    OLED.setCursor(0,0);
    OLED.setTextSize(1); 
    OLED.println("Light: ");
    OLED.setCursor(45,10);
    OLED.setTextSize(3);
    OLED.println(lux);
    OLED.display();
  delay(400);
    
//    else
//    {
//      Serial.println("not thing");
//      OLED.clearDisplay();
//      OLED.setTextSize(3);
//      OLED.println("");
//      OLED.display();
//      delay(500);
//    }
 Serial.print("Light: ");
  Serial.println(lux);

}
