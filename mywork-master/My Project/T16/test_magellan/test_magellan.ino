#include <MQTT.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <ArduinoJson.h>
#include "DHT.h"

////// Ssid&Pass //////
#define ESP_AP_NAME "SET_ME_SSID"
#define ESP_AP_PASS ""

////// PIN //////
#define DHTPIN D5
#define RELAY_D1 D6
#define BUTTON D7
#define DHTTYPE DHT22

////// Key Token //////
//const char* keyToken = "";
//const char* ipMagellan = "";
 const char* keyToken = "692b84b0-ac58-11e9-9aef-8de86578e580";
 const char* ipMagellan = "13.67.108.80";

  ////// Status //////
 volatile int stateA = LOW;
 volatile int stateB = LOW;
 volatile int stateC = LOW;
 volatile int stateD = LOW;
 float Moisture;

  ////// a=topic , b=payload //////
 String a,b;
  
   ////// Send time delay //////
 unsigned long lastMillis = 0;
 unsigned long lastMillis2 = 0;
 unsigned long lastMillisSW1 = 0;
 unsigned long lastMillisSW2 = 0;
 unsigned long lastMillisSW3 = 0;
 unsigned long lastMillisSW4 = 0;
 
  ////// Timer //////
 int TimeoutConnect = 120;
 
  ////// Timer //////
 int Timer1=0;
 int Sw1_10A=0;
 boolean condition_1=false;

DHT dht(DHTPIN, DHTTYPE);
//Ticker ticker;
MQTTClient client(1024);
WiFiClient net;
WiFiManager wifiManager;
StaticJsonDocument<200> doc;

/*--------------------------- Connect -------------------------------*/
void connect() {
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("Offline ");
    ESP.reset();
    delay(10);
  }
  Serial.println("\nconnecting...");
  while (!client.connect("Chocoly", "admin", "admin")) {
    Serial.print(".");
    delay(1000);
  }

//  client.subscribe("monitor/" + String(keyToken));
  client.subscribe("dashboards/data/" + String(keyToken));
   
}

/*--------------------------- messageReceived -------------------------------*/
void messageReceived(String &topic, String &payload ) {
  Serial.println("incoming: " + topic + " - " + payload );
  a=topic;
  b=payload;

    /*####################### Condition ON #############################*/
  if(a =="dashboards/data/" + String(keyToken)){
  DeserializationError error = deserializeJson(doc, b);

  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }

  int Switch1_10A = doc["Switch1_10A"];
  int Timer = doc["Timer"];

   /*############################### Switch 1 #############################################*/
  if(a == "dashboards/data/" + String(keyToken) && b== "{\"Switch1_10A\":1}" ){
    digitalWrite(RELAY_D1, HIGH);
    stateA=1;
  //  client.publish("monitor/" + String(keyToken), " {\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Switch1_10A\",\"Data\":\"" +  String(stateA)  + "\"},{\"Name\":\"BUTTON_2\",\"Data\":\"" +  String(stateB)  + "\"},{\"Name\":\"BUTTON_3\",\"Data\":\"" +  String(stateC)  + "\"},{\"Name\":\"BUTTON_4\",\"Data\":\"" +  String(stateD)  + "\"}]} ");
    Serial.println("SWITCH1 : OPEN");
    delay(1);
    a="";
    b="";
  }
  if(a == "dashboards/data/" + String(keyToken) && b== "{\"Switch1_10A\":0}" ){
    digitalWrite(RELAY_D1, LOW);
    stateA=0;
  //  client.publish("monitor/" + String(keyToken), " {\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Switch1_10A\",\"Data\":\"" +  String(stateA)  + "\"},{\"Name\":\"BUTTON_2\",\"Data\":\"" +  String(stateB)  + "\"},{\"Name\":\"BUTTON_3\",\"Data\":\"" +  String(stateC)  + "\"},{\"Name\":\"BUTTON_4\",\"Data\":\"" +  String(stateD)  + "\"}]} ");
    Serial.println("SWITCH1 : CLOSE");
    delay(1);
    a="";
    b="";
  }

/*################################## Switch timer ################################################################*/  
  
/*s1*/  if((Switch1_10A==1)&&(Timer>0)){
    Timer1=Timer;
    Timer1=Timer1*1000;
    condition_1=true;
    stateA=1;
    digitalWrite(RELAY_D1, HIGH);
    Serial.println("SWITCH1 : OPEN : TIMER1");
    a = "";
    b = "";

     }
   }
}

/*--------------------------- LED AP -------------------------------*/
void tick() {
 int statee = digitalRead(RELAY_D1);
 digitalWrite(RELAY_D1, !statee);
}

/*--------------------------- Setup -------------------------------*/
void setup() {
  Serial.begin(115200);
  while (!Serial) continue;

  pinMode(DHTPIN, INPUT);
  pinMode(RELAY_D1, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);

if(digitalRead(BUTTON) == LOW) // Press button
  {
    //reset saved settings
    wifiManager.resetSettings(); // go to ip 192.168.4.1 to config
    Serial.println("WiFi reset");
  }
  
//  ticker.attach(0.2, tick);

  wifiManager.setTimeout(TimeoutConnect);
  wifiManager.autoConnect(ESP_AP_NAME,ESP_AP_PASS);

  client.begin("13.67.108.80", net);
  client.onMessage(messageReceived);
  connect();
//  ticker.detach();
  dht.begin();
  digitalWrite(RELAY_D1, LOW);
}

/*--------------------------- Loop -------------------------------*/
void loop() {
  
  /*############################### client LOOP ###################################*/
  client.loop();
  delay(10);

  if (!client.connected()) {
    stateA=0;

    digitalWrite(RELAY_D1, LOW);
 //   digitalWrite(RELAY_D1, LOW);
    Serial.println("Offline 2");
    connect();
  }
 

/*####################### Condition OFF #############################*/
if (millis() - lastMillisSW1 > Timer1) {
    lastMillisSW1 = millis();

      if(condition_1==true){
        delay(1);
          stateA=0;
          digitalWrite(RELAY_D1, LOW);
          Serial.println("SWITCH1 : CLOSE : TIMER1");
          Timer1=0;
          Sw1_10A=0;
          condition_1=false;
          a = "";
          b = "";
      }  
  }

  
/*########################## Pub1 only 1 sec #############################*/   
   if (millis() - lastMillis > 5000) {
    lastMillis = millis();
    
        client.publish("monitor/" + String(keyToken), " {\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Switch1_10A\",\"Data\":\"" +  String(stateA)  + "\"}]} ");   

  }

  /*########################## Pub2 only 20 sec #############################*/   
   if (millis() - lastMillis2 > 20000) {
    lastMillis2 = millis();
          char outec[15];
          char outph[15];
          float h = dht.readHumidity();
          float t = dht.readTemperature();
          float ec =random(30.0000);
          dtostrf(ec,2, 4, outec);
          float ph =random(7.0);
          dtostrf(ph,1, 1, outph);
         
        client.publish("monitor/" + String(keyToken), " {\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Temp\",\"Data\":\"" +  String(t)  + "\"},{\"Name\":\"Humi\",\"Data\":\"" +  String(h)  + "\"},{\"Name\":\"EC\",\"Data\":\"" +  String(outec)  + "\"},{\"Name\":\"PH\",\"Data\":\"" +  String(outph)  + "\"}]} ");   
        Serial.println("Send Sensor");
        Serial.println(t);
  }
  
}
