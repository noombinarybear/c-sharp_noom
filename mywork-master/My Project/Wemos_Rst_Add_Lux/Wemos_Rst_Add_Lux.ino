#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <MQTT.h>
#include "DHT.h"
#include <Adafruit_Sensor.h>
#include <WiFiManager.h>
#include <ArduinoJson.h>
#include <BH1750FVI.h>
BH1750FVI LightSensor(BH1750FVI::k_DevModeContLowRes);

////// Ssid&Pass AP MODE //////
#define ESP_AP_NAME "SET_ME_SSID"
#define ESP_AP_PASS ""
/////////////////////////////
#define DHTPIN D5
#define DHTTYPE DHT22
#define LED D6
#define Switch D7  //SW ON OFF
#define SWRST D8  //SW Reset
int State=0;    //Status LED
int ST_SW;    //Status SW ON OFF
//int ST_RST;  //Status SW Reset
DHT dht(DHTPIN, DHTTYPE);
String Subload;
// String timecon;
int Seal=0;
String Tp;
String Pl;

int St1 =LOW;
int SWT;
int Sumtime =0;
bool condition ;
bool ledOn =false;
unsigned long lastMillis_1 =0;
unsigned long lastMillis = 0;

//const char ssid[] = "Orbi The Gentry";
//const char pass[] = "Virus@coth";

//const char ssid[] = "LittleNoom";
//const char pass[] = "ad22334455";


const char* keyToken ="692b84b0-ac58-11e9-9aef-8de86578e580";
const char* ipMagellan ="13.67.108.80";

WiFiManager wifiManager;
WiFiClient net;
MQTTClient client(1024);  //1024คือจำนวนไบต์ ไบต์น้อย ชื่อตรง ESLR ซ้ำจะทำให้โดนเด้งออกจากเซิฟเวอร์
StaticJsonDocument<200> doc;



void connect() {
  Serial.print("checking wifi...");
  
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
  }

  Serial.print("\nconnecting...");
  while (!client.connect("EFDE","admin","admin")) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");
//  Serial.print(ESP_AP_NAME);

//    client.subscribe("/hello");
    client.subscribe("dashboards/data/" +String(keyToken));
  // client.unsubscribe("/hello");
}
///##### switch on off on MCU ##################
void ZWITCH()
      {
       ST_SW = digitalRead(Switch);
//           if(ST_SW == LOW)
//            {
//
//              if(State ==0)
//                 {
//                  digitalWrite(LED,HIGH);
//                  State =1;
//                  client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"1"+"\"},{\"Name\":\"STATE\",\"Data\":\"1"+"\"}]}");
//                  Serial.print("STATE =");
//                  Serial.println(State);
////                    St1=false;
////                  delay(200);
//                 }
//                    else
//                     {
//                      digitalWrite(LED,LOW);
//                      State =0;
//                      client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"0"+"\"},{\"Name\":\"STATE\",\"Data\":\"0"+"\"}]}");
//                      Serial.print("STATE =");
//                      Serial.println(State);
////                      St1=true;
////                      delay(200);
//                     }
//              
//      
//            }

      //#################TEST2 Fix debounce ####################
      if((ST_SW !=St1)&&(St1 ==HIGH))
      {
        ledOn = !ledOn;
        digitalWrite(LED, ledOn);  
        Serial.println(ledOn);
        client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\""+String(ledOn)+""+"\"},{\"Name\":\"STATE\",\"Data\":\""+String(ledOn)+""+"\"}]}");
        State =ledOn;
      }
        St1 = ST_SW;
        
      }   

void messageReceived(String &topic, String &payload)
          {
             Serial.println("incoming: " + topic + " - " + payload);
             //### TP =topic  PL=payload###
             Tp =topic;
             Pl =payload;
        
   
             
              if(Tp =="dashboards/data/" + String(keyToken))
                {
                 DeserializationError error = deserializeJson(doc, Pl);
                }
               SWT = doc["SW"];
               int timecon = doc["Timer"];
              Sumtime =timecon*1000;
             if(topic =="dashboards/data/" + String(keyToken) && payload =="{\"SW\":0}")
              {
                digitalWrite(LED,LOW);
                Serial.println("OFF");
                 State = 0;

//                String load = payload;

//                 client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"0"+"\"},{\"Name\":\"STATE\",\"Data\":\""+String(State)+""+"\"}]}");
              }
             if(topic =="dashboards/data/" + String(keyToken) && payload =="{\"SW\":1}")
              {
                digitalWrite(LED,HIGH);
                Serial.println("ON");
                 State = 1;
//                 client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"1"+"\"},{\"Name\":\"STATE\",\"Data\":\""+String(State)+""+"\"}]}");
              }

              // timecon = doc["Timer"];
              Serial.println(timecon);

       //################### Condition Control ###################
                Serial.println(Pl); //ข้อมูลPayload
                Serial.print("SWJson=");
                Serial.println(SWT);
                Serial.print("time con=");
                Serial.println(timecon);
//              String Subload = payload.substring(7);
             // Subload = payload.substring(9,14);
             // timecon = payload.substring(16,1);
      
      //####### Condition SW ################
          if((SWT ==1) && (timecon >0))
            {
              // Serial.print("GET");
                // timecon ==0;
                condition = true;
                digitalWrite(LED, HIGH);
                Serial.println("ON WITH Condition");
                Tp ="";
                Pl ="";
                State =1;
            }


          }



void setup() 
{
//  pinMode(SWRST,INPUT_PULLUP);
//  pinMode(Switch,INPUT);
   pinMode(Switch,INPUT_PULLUP);
  pinMode(LED,OUTPUT);
  Serial.begin(115200);
  LightSensor.begin();
  dht.begin();
  WiFiManager wifiManager;
//  ############## WDT #######
ESP.wdtDisable();
ESP.wdtEnable(WDTO_8S);
//############################
//  WiFi.begin(ssid, pass);

  if(digitalRead(Switch)== LOW)
  {
  wifiManager.resetSettings();
  Serial.print("WiFi reset");
  }
//   digitalWrite(D4,HIGH);
//    delay(500);
//    digitalWrite(D4,LOW);
//    delay(500);
  wifiManager.setTimeout(180);
  wifiManager.autoConnect(ESP_AP_NAME,ESP_AP_PASS);
    
  // Note: Local domain names (e.g. "Computer.local" on OSX) are not supported by Arduino.
  // You need to set the IP address directly.
  client.begin("13.67.108.80", net);

  client.onMessage(messageReceived);

  connect();
}

void loop() 
{
        uint16_t lux = LightSensor.GetLightIntensity();
        client.loop();
        delay(10);  // <- fixes some issues with WiFi stability

         if (!client.connected()) 
        {
          connect();
        }

  ZWITCH();

    client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\""+String(State)+""+"\"},{\"Name\":\"STATE\",\"Data\":\""+String(State)+""+"\"}]}");
  
    //########### Condition OFF ####################
    
  if(millis() - lastMillis_1 > Sumtime)
      {
        lastMillis_1 = millis();
        if(condition ==true)
              {
                delay(5);
                digitalWrite(LED,LOW);
                Serial.println("Close Timer");
                SWT =0;
                State =0;
                condition = false;
                Tp ="";
                Pl ="";
                Sumtime = 0;
              }
      }
    
  // publish a message roughly every second.
  if(millis() - lastMillis > 5000) 
  {
    lastMillis = millis();
       int a = random(100,200);  //random(ค่า); //random(ค่าจาก,ค่าถึง);
       float h = dht.readHumidity();
       // Read temperature as Celsius (the default)
       float t = dht.readTemperature();
       // Read temperature as Fahrenheit (isFahrenheit = true)
       float f = dht.readTemperature(true);


//    client.publish("/hello", "world");
//    client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Value\",\"Data\":\""+String(a)+""+"\"}]}");
     //Serial.print("Temp=");
    // Serial.println(t);


 //    Serial.println(State);
//     client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Temp\",\"Data\":\""+String(t)+""+"\"}]}");
//     client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"SW\",\"Data\":\"0"+"\"}]}");
//     client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"STATE\",\"Data\":\""+String(State)+""+"\"}]}");
//     client.publish("monitor/" + String(keyToken), "{\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Value\",\"Data\":\""+String(a)+"\"},{\"Name\":\"Temp\",\"Data\":\""+String(t)+"\"},{\"Name\":\"SW\",\"Data\":\""+String(State)+""+"\"},{\"Name\":\"STATE\",\"Data\":\""+String(State)+""+"\"},{\"Name\":\"LUXSENSOR\",\"Data\":\""+ String(lux)+""+"\"}]}");
//  }   
      client.publish("monitor/" + String(keyToken),"{\"DeviceId\":\"" + String(keyToken) +"\",\"Sensors\":[{\"Name\":\"Value\",\"Data\":\""+String(a)+"\"},{\"Name\":\"Temp\",\"Data\":\""+String(t)+"\"},{\"Name\":\"LUXSENSOR\",\"Data\":\""+String(lux)+""+"\"},{\"Name\":\"Huminity\",\"Data\":\""+ String(h)+""+"\"}]}");
  }
  ESP.wdtFeed();
}
