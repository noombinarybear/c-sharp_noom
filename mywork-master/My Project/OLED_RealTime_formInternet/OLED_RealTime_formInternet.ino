#include <ESP8266WiFi.h>
#include <WiFiManager.h>
#include <time.h>
#include <Wire.h> 
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
Adafruit_SSD1306 OLED(-1); 
#define Switch D7  //SW ON OFF


#define ESP_AP_NAME "SET_LITTLE_CLOCK"
#define ESP_AP_PASS ""
//
//const char* ssid = "Orbi The Gentry";                  //ใส่ชื่อ SSID Wifi
//const char* password = "Virus@coth";          //ใส่รหัสผ่าน
WiFiManager wifiManager;
WiFiClient net;
int timezone = 7 * 3600;                    //ตั้งค่า TimeZone ตามเวลาประเทศไทย
int dst = 0;                                //กำหนดค่า Date Swing Time
//OLED.begin(SSD1306_SWITCHCAPVCC,0x3C);
void connect() 
{
             Serial.print("checking wifi...");
             while (WiFi.status() != WL_CONNECTED) 
                {
                  Serial.print(".");
                        OLED.clearDisplay(); 
                        OLED.setTextColor(WHITE,BLACK); 
                        OLED.setCursor(0,0);
                        OLED.setTextSize(2); 
                        OLED.println("Lost Connect");
                        OLED.display(); 
                  delay(1000);
                }
    configTime(timezone, dst, "pool.ntp.org", "time.nist.gov");     //ดึงเวลาจาก Server
    Serial.println("\nWaiting for time");
    while (!time(nullptr)) {
      Serial.print(".");
      delay(1000);
    }
    
}   
void LTclock()
{
            configTime(timezone, dst, "pool.ntp.org", "time.nist.gov");    //ดีงเวลาปัจจุบันจาก Server อีกครั้ง
      time_t now = time(nullptr);
      struct tm* p_tm = localtime(&now);
     //ชม  
       Serial.print("Time:");
      Serial.print(p_tm->tm_hour);
      Serial.print(":"); 
     //นาที
    //   Serial.print("Sec = ");
      Serial.print(p_tm->tm_min);
      Serial.print(":");
      //วิ
    //  Serial.print("Sec = ");
      Serial.println(p_tm->tm_sec);
    //  Serial.println("");
      delay(1000);
      OLED.clearDisplay(); 
      OLED.drawRect(13,15,100,40,WHITE);   //สี่เหลี่ยมเล็ก
      OLED.setTextColor(WHITE,BLACK);   //Text is white ,background is black
      OLED.setCursor(17, 22);
      OLED.setTextSize(3); 
      OLED.println(String(p_tm->tm_hour)+":"+String(p_tm->tm_min)); 
      OLED.setCursor(0,0);
      OLED.setTextSize(1); 
      OLED.println("Time Now:"+String(p_tm->tm_mday)+"/"+String(p_tm->tm_mon)+"/"+String((p_tm->tm_year)+1900));
    //  OLED.println(String((p_tm->tm_year)+1900  ));
      OLED.setCursor(((p_tm->tm_sec)*2),43);
      OLED.setTextSize(2); 
      OLED.println(".");
      OLED.setCursor((128-((p_tm->tm_sec))*2.2),3);
      OLED.setTextSize(2); 
      OLED.println(".");
      OLED.display(); 
      ESP.wdtFeed();
}
void setup()
{
 OLED.begin(SSD1306_SWITCHCAPVCC,0x3C);
 OLED.clearDisplay();
 OLED.setTextColor(WHITE,BLACK); 
 OLED.setCursor(0,0);
 OLED.setTextSize(2); 
 OLED.println("Device");
 OLED.println("Ready");
 OLED.display();
 pinMode(Switch,INPUT_PULLUP);
 WiFiManager wifiManager;
//  ############## WDT #######
ESP.wdtDisable();
ESP.wdtEnable(WDTO_8S);
/// #########################
 if(digitalRead(Switch)== LOW)
  {
  wifiManager.resetSettings();
  Serial.print("WiFi reset");
          OLED.clearDisplay(); 
          OLED.setTextColor(WHITE,BLACK); 
          OLED.setCursor(0,0);
          OLED.setTextSize(2); 
          OLED.println("Config,WiFi,NAME:");
          OLED.println("SET_LITTLE_CLOCK");
          OLED.display(); 
  }

  wifiManager.setTimeout(180);
  wifiManager.autoConnect(ESP_AP_NAME,ESP_AP_PASS);

  /// #########################
  
 Serial.begin(115200);
 Serial.setDebugOutput(true);

//  WiFi.mode(WIFI_STA);                                        //เชื่อมต่อ Wifi
//  WiFi.begin(ssid, password);
  Serial.println("\nConnecting to WiFi");
  OLED.clearDisplay(); 
  OLED.setTextColor(WHITE,BLACK); 
  OLED.setCursor(0,0);
  OLED.setTextSize(2); 
  OLED.println("Connecting..");
  OLED.display();
  while (WiFi.status() != WL_CONNECTED) 
   { 
         
     Serial.print(",");
     delay(1000);
   }
//    configTime(timezone, dst, "pool.ntp.org", "time.nist.gov");     //ดึงเวลาจาก Server
//    Serial.println("\nWaiting for time");
//    while (!time(nullptr)) {
//      Serial.print(".");
//      delay(1000);
//    }
    Serial.println("");
     connect();
}
        
void loop()
{
//      client.loop();
//      delay(10);  // <- fixes some issues with WiFi stability

  if (WiFi.status() != WL_CONNECTED) 
      {

          connect();
      }
   else{
     LTclock();
   
//  configTime(timezone, dst, "pool.ntp.org", "time.nist.gov");    //ดีงเวลาปัจจุบันจาก Server อีกครั้ง
//  time_t now = time(nullptr);
//  struct tm* p_tm = localtime(&now);
// //ชม  
//   Serial.print("Time:");
//  Serial.print(p_tm->tm_hour);
//  Serial.print(":"); 
// //นาที
////   Serial.print("Sec = ");
//  Serial.print(p_tm->tm_min);
//  Serial.print(":");
//  //วิ
////  Serial.print("Sec = ");
//  Serial.println(p_tm->tm_sec);
////  Serial.println("");
//  delay(1000);
  LTclock();

  }
}
