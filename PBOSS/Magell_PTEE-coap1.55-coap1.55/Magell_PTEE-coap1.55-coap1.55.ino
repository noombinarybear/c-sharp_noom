#include <Wire.h>
#include <Arduino.h>
#include "Magellan.h"
Magellan magel;
long randNumber;
char auth[] = "b8699210-09d2-11ea-90fd-a5fd380b223b";   //Token Key you can get from magellan platform
String payload;
String control=0;
void setup() {
  Serial.begin(9600);
  magel.begin(auth);           //init Magellan LIB
  pinMode(LED_BUILTIN, OUTPUT);
//    payload = "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"NUM\",\"Data\":\"" + String(randNumber) + "\"}]}";
//  magel.post(payload);
}
void loop() {
  randNumber = random(50);
  payload = "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"led\",\"Data\":\"" + String(control) + "\"},{\"Name\":\"NUM\",\"Data\":\"" + String(randNumber) + "\"}]}";
  magel.post(payload);
  control = magel.get("Lamp");
  if (control == "1") {
    digitalWrite(LED_BUILTIN, HIGH);
  } else if (control == "0") {
    digitalWrite(LED_BUILTIN, LOW);
  }
    delay(300);
}
