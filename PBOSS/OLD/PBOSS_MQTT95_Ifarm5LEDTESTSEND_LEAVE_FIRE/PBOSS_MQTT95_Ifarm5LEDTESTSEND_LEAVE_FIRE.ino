///// PASS Regist with out reconnect   /////////////IFARM PASS
#include "MagMQTT.h"
#include <EasyButton.h>

#define MT1LED 53
#define MT2LED 52
#define MT3LED 51
#define MT4LED 50
#define MT5LED 49

#define SW1 33
#define SW2 32
#define SW3 31
#define SW4 30
#define SW5 28


EasyButton button1(SW1);
EasyButton button2(SW2);
EasyButton button3(SW3);
EasyButton button4(SW4);
EasyButton button5(SW5);

int status_button_1 = LOW;
int status_button_2 = LOW;
int status_button_3 = LOW;
int status_button_4 = LOW;
int status_button_5 = LOW;

int power1 = LOW;
int power2 = LOW;
int power3 = LOW;
int power4 = LOW;
int power5 = LOW;

unsigned long  prevmillis = 0;
MagMQTT magel;

//char auth[] = "32dfec40-0c0f-11ea-90fd-a5fd380b223b";   //Token Key you can get from magellan platform
char auth[] = "8ccecd00-0cf7-11ea-90fd-a5fd380b223b"; //TESTSEND DATA
//unsigned char username[]="Sim.520039400017475";
unsigned char server[] = "13.67.108.80";
unsigned char port[] = "1883";
unsigned char username[] = "admin";
unsigned char password[] = "admin";
unsigned char clientID[] = "alias";
//int SW1;
////// Sub Control for Get Value ////
//String Subdelta = "SW1";
/////////////////////////////
String MT1 = "0";
String MT2 = "0";
String MT3 = "0";
String MT4 = "0";
String MT5 = "0";
//unsigned char clientID2[]="";
String global_payload;
String global_topic;
String IMSI_;
boolean subscrb = false;
unsigned long millis_ = 0;
unsigned long lastMillis = 0;
unsigned long lastMillis_2 = 0;
unsigned long lastMillis_3 = 0;
String Lamp = "0";
int hour_ = 0;
int minute_ = 0;


void onButton1Pressed()
{
  status_button_1 = HIGH;
  if (status_button_1 == HIGH)
  {
    Serial.println("Button1 has been pressed!");
    power1 = !power1;
    digitalWrite(MT1LED, power1 );
    status_button_1 = LOW;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow1 = power1;
      magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"MT1\",\"Data\":\"" +  String(pow1)  + "\"},{\"Name\":\"LAMPMT1\",\"Data\":\"" +  String(pow1)  + "\"}]}");
    }
  }
}
void onButton2Pressed()
{
  status_button_2 = HIGH;
  if (status_button_2 == HIGH)
  {
    Serial.println("Button2 has been pressed!");
    power2 = !power2;
    digitalWrite(MT2LED, power2 );
    status_button_2 = LOW;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow2 = power2;
      magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"MT2\",\"Data\":\"" +  String(pow2)  + "\"},{\"Name\":\"LAMPMT2\",\"Data\":\"" +  String(pow2)  + "\"}]}");
    }
  }
}
void onButton3Pressed()
{
  status_button_3 = HIGH;
  if (status_button_3 == HIGH)
  {
    Serial.println("Button3 has been pressed!");
    power3 = !power3;
    digitalWrite(MT3LED, power3 );
    status_button_3 = LOW;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow3 = power3;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"MT3\",\"Data\":\"" +  String(pow3)  + "\"},{\"Name\":\"LAMPMT3\",\"Data\":\"" +  String(pow3)  + "\"}]}");
    }
  }
}
void onButton4Pressed()
{
  status_button_4 = HIGH;
  if (status_button_4 == HIGH)
  {
    Serial.println("Button4 has been pressed!");
    power4 = !power4;
    digitalWrite(MT4LED, power4 );
    status_button_4 = LOW;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow4 = power4;
      magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"MT4\",\"Data\":\"" +  String(pow4)  + "\"},{\"Name\":\"LAMPMT4\",\"Data\":\"" +  String(pow4)  + "\"}]}");
    }
  }
}
void onButton5Pressed()
{
  status_button_5 = HIGH;
  if (status_button_5 == HIGH)
  {
    Serial.println("Button5 has been pressed!");
    power5 = !power5;
    digitalWrite(MT5LED, power5 );
    status_button_5 = LOW;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow5 = power5;
      magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"MT5\",\"Data\":\"" +  String(pow5)  + "\"},{\"Name\":\"LAMPMT5\",\"Data\":\"" +  String(pow5)  + "\"}]}");
    }
  }
}




void Subscribe_list()
{
  if (subscrb == false)
  {
    magel.subscribe("dashboards/data/" + String(auth));
    delay(5000);
    subscrb = true;
  }
}


void callback(String &topic, String &payload)
{

  if ((topic != "NULL") && (payload != "NULL") && (topic != NULL) && (payload != NULL) )
  {
    Serial.println("Incoming :" + topic + " : " + payload);
    global_topic = topic;
    global_payload = payload;
  }
  /////////////////////////////////////////////////
  //  if ((global_topic.startsWith("\"dashboards")) && (global_payload != NULL))
  //   if ((global_topic =="\"dashboards/data/"+String(auth)+"\"") && (global_payload != NULL))
  //  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"Switch1_10A\":0}"))
  //  {
  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT1\":1}"))
  {
    digitalWrite(MT1LED, HIGH);
    Serial.println("MT1LED ON");
    MT1 = "1";
    power1 = HIGH;
    String topic = "monitor/" + String(auth);
      if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow1 = power1;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"LAMPMT1\",\"Data\":\"" +  String(pow1)  + "\"}]}");
    }
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT1\":0}"))
  {
    digitalWrite(MT1LED, LOW);
    Serial.println("MT1LED OFF");
    MT1 = "0";
    power1 = LOW;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow1 = power1;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"LAMPMT1\",\"Data\":\"" +  String(pow1)  + "\"}]}");
    }
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT2\":1}"))
  {
    digitalWrite(MT2LED, HIGH);
    Serial.println("MT2LED ON");
    MT2 = "1";
    power2 = HIGH;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow2 = power2;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"LAMPMT2\",\"Data\":\"" +  String(pow2)  + "\"}]}");
    }

  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT2\":0}"))
  {
    digitalWrite(MT2LED, LOW);
    Serial.println("MT2LED OFF");
    MT2 = "0";
    power2 = LOW;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow2 = power2;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"LAMPMT2\",\"Data\":\"" +  String(pow2)  + "\"}]}");
    }
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT3\":1}"))
  {
    digitalWrite(MT3LED, HIGH);
    Serial.println("MT3LED ON");
    MT3 = "1";
    power3 = HIGH;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow3 = power3;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"LAMPMT3\",\"Data\":\"" +  String(pow3)  + "\"}]}");
    }
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT3\":0}"))
  {
    digitalWrite(MT3LED, LOW);
    Serial.println("MT3LED OFF");
    MT3 = "0";
    power3 = LOW;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow3 = power3;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"LAMPMT3\",\"Data\":\"" +  String(pow3)  + "\"}]}");
    }
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT4\":1}"))
  {
    digitalWrite(MT4LED, HIGH);
    Serial.println("MT4LED ON");
    MT4 = "1";
    power4 = HIGH;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow4 = power4;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"LAMPMT4\",\"Data\":\"" +  String(pow4)  + "\"}]}");
    }
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT4\":0}"))
  {
    digitalWrite(MT4LED, LOW);
    Serial.println("MT4LED OFF");
    MT4 = "0";
    power4 = LOW;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow4 = power4;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"LAMPMT4\",\"Data\":\"" +  String(pow4)  + "\"}]}");
    }
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT5\":1}"))
  {
    digitalWrite(MT5LED, HIGH);
    Serial.println("MT5LED ON");
    MT5 = "1";
    power5 = HIGH;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow5 = power5;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"LAMPMT5\",\"Data\":\"" +  String(pow5)  + "\"}]}");
    }
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT5\":0}"))
  {
    digitalWrite(MT5LED, LOW);
    Serial.println("MT5LED OFF");
    MT5 = "0";
    power5 = LOW;
    String topic = "monitor/" + String(auth);
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      int pow5 = power5;
       magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"LAMPMT5\",\"Data\":\"" +  String(pow5)  + "\"}]}");
    }
  }
  global_topic = "";
  global_payload = "";
  //  }
}

void setup() {
  Serial.begin(9600);
  Serial.println(">>>>>>>>>>>>>>>>> Setup NBIoT <<<<<<<<<<<<<<<<<");
  pinMode(MT1LED, OUTPUT);
  digitalWrite(MT1LED, LOW);
  pinMode(MT2LED, OUTPUT);
  digitalWrite(MT2LED, LOW);
  pinMode(MT3LED, OUTPUT);
  digitalWrite(MT3LED, LOW);
  pinMode(MT4LED, OUTPUT);
  digitalWrite(MT4LED, LOW);
  pinMode(MT5LED, OUTPUT);
  digitalWrite(MT5LED, LOW);
  ///////// SW Begin //////////////
  button1.begin();
  button2.begin();
  button3.begin();
  button4.begin();
  button5.begin();
  button1.onPressed(onButton1Pressed);
  button2.onPressed(onButton2Pressed);
  button3.onPressed(onButton3Pressed);
  button4.onPressed(onButton4Pressed);
  button5.onPressed(onButton5Pressed);
  /////////////////////////////////
  //#######  Call IMSI ########

  magel.Setup_Module_IMSI();
  IMSI_ = magel.imsi;
  String user_imsi = "Sim." + IMSI_;
  Serial.println("Username for Register : " + user_imsi);

  //###########################

  magel.server = server;
  magel.port = port;
  magel.username = username;
  magel.password = password;
  magel.clientID = clientID;
  magel.beginMQTT(auth);           //init Magellan LIB Connect to MQTT Magellan V.3
  magel.RegisCallback(callback);

}

void loop()
{
  magel.Loop();

  button1.read();
  button2.read();
  button3.read();
  button4.read();
  button5.read();


  /*########################## Pub only 60 sec #############################*/
  if (millis() - lastMillis > 60000) {
    lastMillis = millis();
    int stateA = int(random(0, 50));
    int stateB = int(random(0, 100));
    int stateC = int(random(20, 50));
    int stateD = int(random(0, 50));
    int stateE = int(random(0, 50));
    int stateF = int(random(0, 100));
    int stateG = int(random(20, 50));
    int stateH = int(random(0, 50));
    int stateI = int(random(0, 50));
    int Moisture = int(random(10, 150));
    String topic = "monitor/" + String(auth);
    //   magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"Switch1_10A\",\"Data\":\"" +  String(stateA)  + "\"},{\"Name\":\"Switch2_10A\",\"Data\":\"" +  String(stateB)  + "\"},{\"Name\":\"Switch3_10A\",\"Data\":\"" +  String(stateC)  + "\"},{\"Name\":\"Switch4_40A\",\"Data\":\"" +  String(stateD)  + "\"},{\"Name\":\"Moisture\",\"Data\":\"" +  String(Moisture)  + "\"},{\"Name\":\"Switch5_10A\",\"Data\":\"" +  String(stateE)  + "\"},{\"Name\":\"Switch6_10A\",\"Data\":\"" +  String(stateF)  + "\"},{\"Name\":\"Switch7_10A\",\"Data\":\"" +  String(stateG)  + "\"},{\"Name\":\"Switch8_10A\",\"Data\":\"" +  String(stateH)  + "\"},{\"Name\":\"Switch9_10A\",\"Data\":\"" +  String(stateI)  + "\"},{\"Name\":\"LAMPMT1\",\"Data\":\"" + MT1 + "\"},{\"Name\":\"LAMPMT2\",\"Data\":\"" + MT2 + "\"},{\"Name\":\"LAMPMT3\",\"Data\":\"" + MT3 + "\"},{\"Name\":\"LAMPMT4\",\"Data\":\"" + MT4 + "\"},{\"Name\":\"LAMPMT5\",\"Data\":\"" + MT5 + "\"}]}");
    magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"Switch1_10A\",\"Data\":\"" +  String(stateA)  + "\"},{\"Name\":\"Switch2_10A\",\"Data\":\"" +  String(stateB)  + "\"},{\"Name\":\"Switch3_10A\",\"Data\":\"" +  String(stateC)  + "\"},{\"Name\":\"Switch4_40A\",\"Data\":\"" +  String(stateD)  + "\"},{\"Name\":\"Moisture\",\"Data\":\"" +  String(Moisture)  + "\"},{\"Name\":\"Switch5_10A\",\"Data\":\"" +  String(stateE)  + "\"},{\"Name\":\"Switch6_10A\",\"Data\":\"" +  String(stateF)  + "\"},{\"Name\":\"Switch7_10A\",\"Data\":\"" +  String(stateG)  + "\"},{\"Name\":\"Switch8_10A\",\"Data\":\"" +  String(stateH)  + "\"},{\"Name\":\"Switch9_10A\",\"Data\":\"" +  String(stateI)  + "\"}]}");
  }


  /*########################## Pub only 30 sec #############################*/
  //  if (millis() - lastMillis_2 > 30000) {
  //    lastMillis_2 = millis();
  //    int stateA = int(random(0, 50));
  //    int stateB = int(random(0, 100));
  //    int stateC = int(random(20, 50));
  //    int stateD = int(random(0, 50));
  //    int stateE = int(random(0, 50));
  //    int stateF = int(random(0, 100));
  //    int stateG = int(random(20, 50));
  //    int stateH = int(random(0, 50));
  //    int stateI = int(random(0, 50));
  //    int Moisture = int(random(10, 150));
  //    String topic = "monitor/" + String(auth);
  //    magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"Switch1_10A\",\"Data\":\"" +  String(stateA)  + "\"},{\"Name\":\"Switch2_10A\",\"Data\":\"" +  String(stateB)  + "\"},{\"Name\":\"Switch3_10A\",\"Data\":\"" +  String(stateC)  + "\"},{\"Name\":\"Switch4_40A\",\"Data\":\"" +  String(stateD)  + "\"},{\"Name\":\"Moisture\",\"Data\":\"" +  String(Moisture)  + "\"},{\"Name\":\"Switch5_10A\",\"Data\":\"" +  String(stateE)  + "\"},{\"Name\":\"Switch6_10A\",\"Data\":\"" +  String(stateF)  + "\"},{\"Name\":\"Switch7_10A\",\"Data\":\"" +  String(stateG)  + "\"},{\"Name\":\"Switch8_10A\",\"Data\":\"" +  String(stateH)  + "\"},{\"Name\":\"Switch9_10A\",\"Data\":\"" +  String(stateI)  + "\"},{\"Name\":\"LAMPMT1\",\"Data\":\"" + MT1 + "\"},{\"Name\":\"LAMPMT2\",\"Data\":\"" + MT2 + "\"},{\"Name\":\"LAMPMT3\",\"Data\":\"" + MT3 + "\"},{\"Name\":\"LAMPMT4\",\"Data\":\"" + MT4 + "\"},{\"Name\":\"LAMPMT5\",\"Data\":\"" + MT5 + "\"}]}");
  //  }

  if (subscrb == false)
  {
    delay(5000);
    Subscribe_list();
    delay(5000);
  }
}
