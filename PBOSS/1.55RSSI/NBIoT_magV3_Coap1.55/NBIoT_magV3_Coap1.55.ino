#include <Wire.h>
#include <Arduino.h>
#include "Magellan.h"
Magellan magel;
long randNumber;
char auth[] = "b8699210-09d2-11ea-90fd-a5fd380b223b";   //Token Key you can get from magellan platform
String payload;
String control;
String _control2;
String Rssi;
#define LED2 53
void setup() {
  Serial.begin(9600);
  magel.begin(auth);           //init Magellan LIB
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LED2, OUTPUT);
  delay(2000);
  Rssi = magel.rssi();
  Serial.println("RSSI :" + Rssi);
  measurementRSSI();
  payload = "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"Lamp\",\"Data\":\"0\"},{\"Name\":\"Lamp2\",\"Data\":\"0\"}]}";
  magel.post(payload);
}
void loop() {
  randNumber = random(50);
  payload = "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"led2\",\"Data\":\"" + _control2 + "\"},{\"Name\":\"led\",\"Data\":\"" + control + "\"},{\"Name\":\"NUM\",\"Data\":\"" + String(randNumber) + "\"}]}";
  magel.post(payload);
  control = magel.get("Lamp", auth);
  
  if (control == "1") {
    digitalWrite(LED_BUILTIN, HIGH);
  } else if (control == "0") {
    digitalWrite(LED_BUILTIN, LOW);
  }
  
  _control2 = magel.get("Lamp2", auth);
  if (_control2 == "1") {
    digitalWrite(LED2, HIGH);
  } else if (_control2 == "0") {
    digitalWrite(LED2, LOW);
  }
  delay(500);
}

void measurementRSSI()
{
  int intRssi = Rssi.toInt();
  if (intRssi >= -65 )
  {
    Serial.println("Signal Quality : Excellent!");
  }
  else if (intRssi >= -75)
  {
    Serial.println("Signal Quality : Good!");

  }
  else if (intRssi >= -85)
  {
    Serial.println("Signal Quality : Fair!");

  }
  else if (intRssi >= -95)
  {
    Serial.println("Signal Quality : Poor!");

  }
  else if (intRssi <= -95)
  {
    Serial.println("Signal Quality : Disconnect!");
  }
  else if (intRssi == 404)
  {
    Serial.println("Signal Quality : Can't Detect Signal !!");
  }
}
