///// PASS Regist with out reconnect   /////////////IFARM PASS
#include "MagMQTT.h"
#include <ArduinoJson.h>

#define WLED 53
#define YLED 13
MagMQTT magel;

char auth[] = "32dfec40-0c0f-11ea-90fd-a5fd380b223b";   //Token Key you can get from magellan platform

//unsigned char username[]="Sim.520039400017475";
unsigned char server[] = "13.67.108.80";
unsigned char port[] = "1883";
unsigned char username[] = "admin";
unsigned char password[] = "admin";
unsigned char clientID[] = "alias";
int SW1;
////// Sub Control for Get Value ////
//String Subdelta = "SW1";
/////////////////////////////

//unsigned char clientID2[]="";
String global_payload;
String global_topic;
String IMSI_;
boolean token_recv = false;
boolean subscrb = false;
unsigned long millis_ = 0;
unsigned long lastMillis = 0;
unsigned long lastMillis2 = 0;
String Lamp = "0";
int hour_ = 0;
int minute_ = 0;
String Token_Key;
StaticJsonDocument<200> doc;
//char username[50];

//#define LED 13


//void get_token_fromregister()
//{
//  if (token_recv == false)
//  {
//    if (millis() - millis_ > 10000)
//    {
//      millis_ = millis();
//      magel.publish("register/update/sim/v1/" + IMSI_ + "/98.207.254.136", "");
//    }
//  }
//}

void Subscribe_list()
{
  if (subscrb == false)
  {
    magel.subscribe("dashboards/data/" + String(auth));
    delay(5000);
    subscrb = true;
  }
}


void callback(String &topic, String &payload)
{

  if ((topic != "NULL") && (payload != "NULL") && (topic != NULL) && (payload != NULL) )
  {
    Serial.println("Incoming :" + topic + " : " + payload);
    global_topic = topic;
    global_payload = payload;


  }
  /////////////////////////////////////////////////
//  if ((global_topic.startsWith("\"dashboards")) && (global_payload != NULL))
   if ((global_topic =="\"dashboards/data/"+String(auth)+"\"") && (global_payload != NULL))
  {
    DeserializationError error = deserializeJson(doc, global_payload);
    SW1 = doc["SW1"];
    if (SW1 == 1)
    {
      digitalWrite(WLED, HIGH);
      Serial.println("LED ON");
    }
    else if (SW1 == 0)
    {
      digitalWrite(WLED, LOW);
      Serial.println("LED OFF");
    }

  }
}

void setup() {
  Serial.begin(9600);
  Serial.println(">>>>>>>>>>>>>>>>> Setup NBIoT <<<<<<<<<<<<<<<<<");
  pinMode(WLED, OUTPUT);
  digitalWrite(WLED, LOW);
  //#######  Call IMSI ########

  magel.Setup_Module_IMSI();
  IMSI_ = magel.imsi;
  String user_imsi = "Sim." + IMSI_;
  Serial.println("Username for Register : " + user_imsi);

  //###########################

  magel.server = server;
  magel.port = port;
  magel.username = username;
  magel.password = password;
  magel.clientID = clientID;
  magel.beginMQTT(auth);           //init Magellan LIB Connect to MQTT Magellan V.3
  magel.RegisCallback(callback);
  //#########Subscribe for register token with IMsSI to Magellan V.3########
  //  Serial.println("Register SIM to Magellan V.3");
  //  magel.subscribe("register/get/sim/v1/" + IMSI_);
  //  delay(10000);
  //#########################################################
}

void loop()
{
  // put your main code here, to run repeatedly:
  magel.Loop();
  //  get_token_fromregister();
  if (millis() - lastMillis > 10000)
  {
    lastMillis = millis();
    String randdom = String(random(0, 100));
    minute_++;
    String topic = "monitor/" + String(auth);
    String payload = "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"TempBG95\",\"Data\":\"" + randdom + "\"},{\"Name\":\"HumB95\",\"Data\":\"" + randdom + "\"}]}";
    magel.publish(topic, payload);
  }

  if (subscrb == false)
  {
    delay(5000);
    Subscribe_list();
    delay(5000);
  }
}
