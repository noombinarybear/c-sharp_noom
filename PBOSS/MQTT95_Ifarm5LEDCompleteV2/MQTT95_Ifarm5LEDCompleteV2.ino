///// PASS Regist with out reconnect   /////////////IFARM PASS
#include "MagMQTT.h"

#define MT1LED 53
#define MT2LED 52
#define MT3LED 51
#define MT4LED 50
#define MT5LED 49
MagMQTT magel;

char auth[] = "32dfec40-0c0f-11ea-90fd-a5fd380b223b";   //Token Key you can get from magellan platform

//unsigned char username[]="Sim.520039400017475";
unsigned char server[] = "13.67.108.80";
unsigned char port[] = "1883";
unsigned char username[] = "admin";
unsigned char password[] = "admin";
unsigned char clientID[] = "alias";
//int SW1;
////// Sub Control for Get Value ////
//String Subdelta = "SW1";
/////////////////////////////

//unsigned char clientID2[]="";
String global_payload;
String global_topic;
String IMSI_;
boolean token_recv = false;
boolean subscrb = false;
unsigned long millis_ = 0;
unsigned long lastMillis = 0;
unsigned long lastMillis2 = 0;
String Lamp = "0";
int hour_ = 0;
int minute_ = 0;
String Token_Key;

void Subscribe_list()
{
  if (subscrb == false)
  {
    magel.subscribe("dashboards/data/" + String(auth));
    delay(5000);
    subscrb = true;
  }
}


void callback(String &topic, String &payload)
{

  if ((topic != "NULL") && (payload != "NULL") && (topic != NULL) && (payload != NULL) )
  {
    Serial.println("Incoming :" + topic + " : " + payload);
    global_topic = topic;
    global_payload = payload;
  }
  /////////////////////////////////////////////////
  //  if ((global_topic.startsWith("\"dashboards")) && (global_payload != NULL))
  //   if ((global_topic =="\"dashboards/data/"+String(auth)+"\"") && (global_payload != NULL))
  //  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"Switch1_10A\":0}"))
  //  {
  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT1\":1}"))
  {
    digitalWrite(MT1LED, HIGH);
    Serial.println("MT1LED ON");
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT1\":0}"))
  {
    digitalWrite(MT1LED, LOW);
    Serial.println("MT1LED OFF");
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT2\":1}"))
  {
    digitalWrite(MT2LED, HIGH);
    Serial.println("MT2LED ON");
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT2\":0}"))
  {
    digitalWrite(MT2LED, LOW);
    Serial.println("MT2LED OFF");
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT3\":1}"))
  {
    digitalWrite(MT3LED, HIGH);
    Serial.println("MT3LED ON");
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT3\":0}"))
  {
    digitalWrite(MT3LED, LOW);
    Serial.println("MT3LED OFF");
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT4\":1}"))
  {
    digitalWrite(MT4LED, HIGH);
    Serial.println("MT4LED ON");
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT4\":0}"))
  {
    digitalWrite(MT4LED, LOW);
    Serial.println("MT4LED OFF");
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT5\":1}"))
  {
    digitalWrite(MT5LED, HIGH);
    Serial.println("MT5LED ON");
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT5\":0}"))
  {
    digitalWrite(MT5LED, LOW);
    Serial.println("MT5LED OFF");
  }
  global_topic = "";
  global_payload = "";
  //  }
}

void setup() {
  Serial.begin(9600);
  Serial.println(">>>>>>>>>>>>>>>>> Setup NBIoT <<<<<<<<<<<<<<<<<");
  pinMode(MT1LED, OUTPUT);
  digitalWrite(MT1LED, LOW);
  pinMode(MT2LED, OUTPUT);
  digitalWrite(MT2LED, LOW);
  pinMode(MT3LED, OUTPUT);
  digitalWrite(MT3LED, LOW);
  pinMode(MT4LED, OUTPUT);
  digitalWrite(MT4LED, LOW);
  pinMode(MT5LED, OUTPUT);
  digitalWrite(MT5LED, LOW);
  //#######  Call IMSI ########

  magel.Setup_Module_IMSI();
  IMSI_ = magel.imsi;
  String user_imsi = "Sim." + IMSI_;
  Serial.println("Username for Register : " + user_imsi);

  //###########################

  magel.server = server;
  magel.port = port;
  magel.username = username;
  magel.password = password;
  magel.clientID = clientID;
  magel.beginMQTT(auth);           //init Magellan LIB Connect to MQTT Magellan V.3
  magel.RegisCallback(callback);

}

void loop()
{
  magel.Loop();
  if (millis() - lastMillis > 10000)
  {
    lastMillis = millis();
    String randdom = String(random(0, 100));
    minute_++;
    String topic = "monitor/" + String(auth);
    String payload = "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"TempBG95\",\"Data\":\"" + randdom + "\"},{\"Name\":\"HumB95\",\"Data\":\"" + randdom + "\"}]}";
    magel.publish(topic, payload);
  }

  if (subscrb == false)
  {
    delay(5000);
    Subscribe_list();
    delay(5000);
  }
}
