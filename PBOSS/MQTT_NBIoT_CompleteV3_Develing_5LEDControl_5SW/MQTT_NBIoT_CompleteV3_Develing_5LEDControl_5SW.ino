///// PASS Regist with out reconnect
//#######25/11/62  ADDLED5 And SW ///////

#include "MagMQTT.h"
#include <EasyButton.h>
#define MT1LED 53
#define MT2LED 52
#define MT3LED 51
#define MT4LED 50
#define MT5LED 49

//############# BUTTON ##########
#define SW1 33
#define SW2 32
#define SW3 31
#define SW4 30
#define SW5 28

EasyButton button1(SW1);
EasyButton button2(SW2);
EasyButton button3(SW3);
EasyButton button4(SW4);
EasyButton button5(SW5);

int status_button_1 = LOW;
int status_button_2 = LOW;
int status_button_3 = LOW;
int status_button_4 = LOW;
int status_button_5 = LOW;

int power1 = LOW;
int power2 = LOW;
int power3 = LOW;
int power4 = LOW;
int power5 = LOW;
unsigned long  prevmillis = 0;
//#################################


MagMQTT magel;

char auth[] = "your-key";   //Token Key you can get from magellan platform

//unsigned char username[]="Sim.520039400017475";
unsigned char server[] = "13.76.29.21";
unsigned char port[] = "1883";

unsigned char password[] = "19.93.23.2";
unsigned char clientID[] = "alias2";

////// Sub Control for Get Value ////
String Subdelta1 = "Lamp1";
String Subdelta2 = "Lamp2";
String Subdelta3 = "Lamp3";
String Subdelta4 = "Lamp4";
String Subdelta5 = "Lamp5";

String Lamp1 = "0";
String Lamp2 = "0";
String Lamp3 = "0";
String Lamp4 = "0";
String Lamp5 = "0";
/////////////////////////////

//unsigned char clientID2[]="";
String global_payload;
String global_topic;
String IMSI_;
boolean token_recv = false;
boolean subscrb = false;
unsigned long millis_ = 0;
unsigned long lastMillis = 0;
unsigned long lastMillis2 = 0;

int hour_ = 0;
int minute_ = 0;
String Token_Key;
char username[50];

//#define LED 13


void get_token_fromregister()
{
  if (token_recv == false)
  {
    if (millis() - millis_ > 10000)
    {
      millis_ = millis();
      magel.publish("register/update/sim/v1/" + IMSI_ + "/98.207.254.136", "");
    }
  }
}

void Subscribe_list()
{
  if (subscrb == false)
  {
    delay(5000);
    magel.subscribe("delta/get/" + Token_Key + "/" + Subdelta1);
    delay(5000);
    magel.subscribe("delta/get/" + Token_Key + "/" + Subdelta2);
    delay(5000);
    magel.subscribe("delta/get/" + Token_Key + "/" + Subdelta3);
    delay(5000);
    magel.subscribe("delta/get/" + Token_Key + "/" + Subdelta4);
    delay(5000);
    magel.subscribe("delta/get/" + Token_Key + "/" + Subdelta5);
    delay(5000);
    subscrb = true;
  }
}


void callback(String &topic, String &payload)
{

  if ((topic != "NULL") && (payload != "NULL") && (topic != NULL) && (payload != NULL) )
  {
    Serial.println("Incoming :" + topic + " : " + payload);
    global_topic = topic;
    global_payload = payload;
    ///////////Get Token Key Magellan V3 ////////////
    if ((global_topic == "\"register/get/sim/v1/" + IMSI_ + "\"" ) && (global_payload != NULL))
    {
      Serial.println("This My Token : " + global_payload);
      Token_Key = global_payload;
      delay(5000);
      //        Serial.println("Got Token!! Reconnecting MQTT...");
      //        re_open_mqtt();
      token_recv = true;
    }
    /////////////////////////////////////////////////
    if ((global_topic == "\"delta/get/" + Token_Key + "/" + Subdelta1 + "\"" ) && (global_payload != NULL))
    {
      if (global_payload == "1")
      {
        digitalWrite(MT1LED, HIGH);
        Lamp1 = "1";
        if (millis() - prevmillis > 1000)
        {
          prevmillis = millis();
          String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
          String payload = "{\"Lamp1\":\"" + Lamp1 + "\"}";
          magel.publish(topic, payload);
        }
      }
      else if (global_payload == "0")
      {
        digitalWrite(MT1LED, LOW);
        Lamp1 = "0";
        if (millis() - prevmillis > 1000)
        {
          prevmillis = millis();
          String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
          String payload = "{\"Lamp1\":\"" + Lamp1 + "\"}";
          magel.publish(topic, payload);
        }
      }

    }
    if ((global_topic == "\"delta/get/" + Token_Key + "/" + Subdelta2 + "\"" ) && (global_payload != NULL))
    {
      if (global_payload == "1")
      {
        digitalWrite(MT2LED, HIGH);
        Lamp2 = "1";
        if (millis() - prevmillis > 1000)
        {
          prevmillis = millis();
          String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
          String payload = "{\"Lamp2\":\"" + Lamp2 + "\"}";
          magel.publish(topic, payload);
        }
      }
      else if (global_payload == "0")
      {
        digitalWrite(MT2LED, LOW);
        Lamp2 = "0";
        if (millis() - prevmillis > 1000)
        {
          prevmillis = millis();
          String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
          String payload = "{\"Lamp2\":\"" + Lamp2 + "\"}";
          magel.publish(topic, payload);
        }
      }

    }
    if ((global_topic == "\"delta/get/" + Token_Key + "/" + Subdelta3 + "\"" ) && (global_payload != NULL))
    {
      if (global_payload == "1")
      {
        digitalWrite(MT3LED, HIGH);
        Lamp3 = "1";
        if (millis() - prevmillis > 1000)
        {
          prevmillis = millis();
          String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
          String payload = "{\"Lamp3\":\"" + Lamp3 + "\"}";
          magel.publish(topic, payload);
        }
      }
      else if (global_payload == "0")
      {
        digitalWrite(MT3LED, LOW);
        Lamp3 = "0";
        if (millis() - prevmillis > 1000)
        {
          prevmillis = millis();
          String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
          String payload = "{\"Lamp3\":\"" + Lamp3 + "\"}";
          magel.publish(topic, payload);
        }
      }

    }
    if ((global_topic == "\"delta/get/" + Token_Key + "/" + Subdelta4 + "\"" ) && (global_payload != NULL))
    {
      if (global_payload == "1")
      {
        digitalWrite(MT4LED, HIGH);
        Lamp4 = "1";
        if (millis() - prevmillis > 1000)
        {
          prevmillis = millis();
          String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
          String payload = "{\"Lamp4\":\"" + Lamp4 + "\"}";
          magel.publish(topic, payload);
        }
      }
      else if (global_payload == "0")
      {
        digitalWrite(MT4LED, LOW);
        Lamp4 = "0";
        if (millis() - prevmillis > 1000)
        {
          prevmillis = millis();
          String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
          String payload = "{\"Lamp4\":\"" + Lamp4 + "\"}";
          magel.publish(topic, payload);
        }
      }

    }
    if ((global_topic == "\"delta/get/" + Token_Key + "/" + Subdelta5 + "\"" ) && (global_payload != NULL))
    {
      if (global_payload == "1")
      {
        digitalWrite(MT5LED, HIGH);
        Lamp5 = "1";
        if (millis() - prevmillis > 1000)
        {
          prevmillis = millis();
          String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
          String payload = "{\"Lamp5\":\"" + Lamp5 + "\"}";
          magel.publish(topic, payload);
        }
      }
      else if (global_payload == "0")
      {
        digitalWrite(MT5LED, LOW);
        Lamp5 = "0";
        if (millis() - prevmillis > 1000)
        {
          prevmillis = millis();
          String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
          String payload = "{\"Lamp5\":\"" + Lamp5 + "\"}";
          magel.publish(topic, payload);
        }
      }

    }
    global_topic = "";
    global_payload = "";
  }
}
void setup() {
  Serial.begin(9600);
  Serial.println(">>>>>>>>>>>>>>>>> Setup NBIoT <<<<<<<<<<<<<<<<<");
  pinMode(MT1LED, OUTPUT);
  digitalWrite(MT1LED, LOW);
  pinMode(MT2LED, OUTPUT);
  digitalWrite(MT2LED, LOW);
  pinMode(MT3LED, OUTPUT);
  digitalWrite(MT3LED, LOW);
  pinMode(MT4LED, OUTPUT);
  digitalWrite(MT4LED, LOW);
  pinMode(MT5LED, OUTPUT);
  digitalWrite(MT5LED, LOW);
  //#######  BUTTON #########
  button1.begin();
  button2.begin();
  button3.begin();
  button4.begin();
  button5.begin();
  button1.onPressed(onButton1Pressed);
  button2.onPressed(onButton2Pressed);
  button3.onPressed(onButton3Pressed);
  button4.onPressed(onButton4Pressed);
  button5.onPressed(onButton5Pressed);

  //#######  Call IMSI ########
  magel.Setup_Module_IMSI();
  IMSI_ = magel.imsi;
  String user_imsi = "Sim." + IMSI_;
  Serial.println("Username for Register : " + user_imsi);
  //  char username[50];
  user_imsi.toCharArray(username, 50) ;
  //###########################


  magel.server = server;
  magel.port = port;
  magel.username = username;
  magel.password = password;
  magel.clientID = clientID;

  magel.beginMQTT(auth);           //init Magellan LIB Connect to MQTT Magellan V.3

  magel.RegisCallback(callback);


  //#########Subscribe for register token with IMsSI to Magellan V.3########
  Serial.println("Register SIM to Magellan V.3");
  magel.subscribe("register/get/sim/v1/" + IMSI_);

  delay(10000);

  //#########################################################
}

void loop() {

  magel.Loop();
  get_token_fromregister();
  button1.read();
  button2.read();
  button3.read();
  button4.read();
  button5.read();
  if ((Token_Key != NULL) &&  (token_recv == true))
  {

    if (millis() - lastMillis > 10000)
    {
      lastMillis = millis();
      String randdom = String(random(0, 100));
      String humid = String(random(0, 100));
      String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
      String payload = "{\"randomsa\":" + randdom + ",\"Humid\":" + humid + "}";
      magel.publish(topic, payload);
    }


    Subscribe_list();


  }
}
/////////////########################################BUTTON 5 ####################
void onButton1Pressed()
{
  status_button_1 = HIGH;
  if (status_button_1 == HIGH)
  {
    Serial.println("Button1 has been pressed!");
    power1 = !power1;
    digitalWrite(MT1LED, power1 );
    status_button_1 = LOW;
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
      String payload = "{\"Lamp1\":\"" + String(power1) + "\"}";
      magel.publish(topic, payload);
    }
  }
}
void onButton2Pressed()
{
  status_button_2 = HIGH;
  if (status_button_2 == HIGH)
  {
    Serial.println("Button2 has been pressed!");
    power2 = !power2;
    digitalWrite(MT2LED, power2 );
    status_button_2 = LOW;
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
      String payload = "{\"Lamp2\":\"" + String(power2) + "\"}";
      magel.publish(topic, payload);
    }
  }
}
void onButton3Pressed()
{
  status_button_3 = HIGH;
  if (status_button_3 == HIGH)
  {
    Serial.println("Button3 has been pressed!");
    power3 = !power3;
    digitalWrite(MT3LED, power3 );
    status_button_3 = LOW;
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
      String payload = "{\"Lamp3\":\"" + String(power3) + "\"}";
      magel.publish(topic, payload);
    }
  }
}
void onButton4Pressed()
{
  status_button_4 = HIGH;
  if (status_button_4 == HIGH)
  {
    Serial.println("Button4 has been pressed!");
    power4 = !power4;
    digitalWrite(MT4LED, power4 );
    status_button_4 = LOW;
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
      String payload = "{\"Lamp4\":\"" + String(power4) + "\"}";
      magel.publish(topic, payload);
    }
  }
}
void onButton5Pressed()
{
  status_button_5 = HIGH;
  if (status_button_5 == HIGH)
  {
    Serial.println("Button5 has been pressed!");
    power5 = !power5;
    digitalWrite(MT5LED, power5 );
    status_button_5 = LOW;
    if (millis() - prevmillis > 1000)
    {
      prevmillis = millis();
      String topic = "report/update/sim/v1/" + Token_Key + "/98.207.254.136";
      String payload = "{\"Lamp5\":\"" + String(power5) + "\"}";
      magel.publish(topic, payload);
    }
  }
}
