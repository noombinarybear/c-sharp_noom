///// PASS Regist with out reconnect   /////////////IFARM PASS
#include "MagMQTT.h"

#define MT1LED 53
#define MT2LED 52
#define MT3LED 51
#define MT4LED 50
#define MT5LED 49
MagMQTT magel;

//char auth[] = "32dfec40-0c0f-11ea-90fd-a5fd380b223b";   //Token Key you can get from magellan platform
char auth[] = "8ccecd00-0cf7-11ea-90fd-a5fd380b223b"; //TESTSEND DATA
//unsigned char username[]="Sim.520039400017475";
unsigned char server[] = "13.67.108.80";
unsigned char port[] = "1883";
unsigned char username[] = "admin";
unsigned char password[] = "admin";
unsigned char clientID[] = "alias";
//int SW1;
////// Sub Control for Get Value ////
//String Subdelta = "SW1";
/////////////////////////////
String MT1 = "0";
String MT2 = "0";
String MT3 = "0";
String MT4 = "0";
String MT5 = "0";
//unsigned char clientID2[]="";
String global_payload;
String global_topic;
String IMSI_;
boolean subscrb = false;
unsigned long millis_ = 0;
unsigned long lastMillis = 0;
unsigned long lastMillis_2 = 0;
unsigned long lastMillis_3 = 0;
String Lamp = "0";
int hour_ = 0;
int minute_ = 0;


void Subscribe_list()
{
  if (subscrb == false)
  {
    magel.subscribe("dashboards/data/" + String(auth));
    delay(5000);
    subscrb = true;
  }
}


void callback(String &topic, String &payload)
{

  if ((topic != "NULL") && (payload != "NULL") && (topic != NULL) && (payload != NULL) )
  {
    Serial.println("Incoming :" + topic + " : " + payload);
    global_topic = topic;
    global_payload = payload;
  }
  /////////////////////////////////////////////////
  //  if ((global_topic.startsWith("\"dashboards")) && (global_payload != NULL))
  //   if ((global_topic =="\"dashboards/data/"+String(auth)+"\"") && (global_payload != NULL))
  //  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"Switch1_10A\":0}"))
  //  {
  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT1\":1}"))
  {
    digitalWrite(MT1LED, HIGH);
    Serial.println("MT1LED ON");
    MT1 = "1";
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT1\":0}"))
  {
    digitalWrite(MT1LED, LOW);
    Serial.println("MT1LED OFF");
    MT1 = "0";
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT2\":1}"))
  {
    digitalWrite(MT2LED, HIGH);
    Serial.println("MT2LED ON");
    MT2 = "1";

  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT2\":0}"))
  {
    digitalWrite(MT2LED, LOW);
    Serial.println("MT2LED OFF");
    MT2 = "0";
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT3\":1}"))
  {
    digitalWrite(MT3LED, HIGH);
    Serial.println("MT3LED ON");
    MT3 = "1";
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT3\":0}"))
  {
    digitalWrite(MT3LED, LOW);
    Serial.println("MT3LED OFF");
    MT3 = "0";
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT4\":1}"))
  {
    digitalWrite(MT4LED, HIGH);
    Serial.println("MT4LED ON");
    MT4 = "1";
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT4\":0}"))
  {
    digitalWrite(MT4LED, LOW);
    Serial.println("MT4LED OFF");
    MT4 = "0";
  }


  if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT5\":1}"))
  {
    digitalWrite(MT5LED, HIGH);
    Serial.println("MT5LED ON");
    MT5 = "1";
  }
  else if ((global_topic == "\"dashboards/data/" + String(auth) + "\"") && (global_payload == "{\"MT5\":0}"))
  {
    digitalWrite(MT5LED, LOW);
    Serial.println("MT5LED OFF");
    MT5 = "0";
  }
  global_topic = "";
  global_payload = "";
  //  }
}

void setup() {
  Serial.begin(9600);
  Serial.println(">>>>>>>>>>>>>>>>> Setup NBIoT <<<<<<<<<<<<<<<<<");
  pinMode(MT1LED, OUTPUT);
  digitalWrite(MT1LED, LOW);
  pinMode(MT2LED, OUTPUT);
  digitalWrite(MT2LED, LOW);
  pinMode(MT3LED, OUTPUT);
  digitalWrite(MT3LED, LOW);
  pinMode(MT4LED, OUTPUT);
  digitalWrite(MT4LED, LOW);
  pinMode(MT5LED, OUTPUT);
  digitalWrite(MT5LED, LOW);
  //#######  Call IMSI ########

  magel.Setup_Module_IMSI();
  IMSI_ = magel.imsi;
  String user_imsi = "Sim." + IMSI_;
  Serial.println("Username for Register : " + user_imsi);

  //###########################

  magel.server = server;
  magel.port = port;
  magel.username = username;
  magel.password = password;
  magel.clientID = clientID;
  magel.beginMQTT(auth);           //init Magellan LIB Connect to MQTT Magellan V.3
  magel.RegisCallback(callback);

}

void loop()
{
  magel.Loop();
  //  if (millis() - lastMillis > 10000)
  //  {
  //    lastMillis = millis();
  //    String randdom = String(random(0, 100));
  //    minute_++;
  //    String topic = "monitor/" + String(auth);
  //    String payload = "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"TempBG95\",\"Data\":\"" + randdom + "\"},{\"Name\":\"HumB95\",\"Data\":\"" + randdom + "\"}]}";
  //    magel.publish(topic, payload);
  //  }


  /*########################## Pub only 60 sec #############################*/
  if (millis() - lastMillis > 60000) {
    lastMillis = millis();
    int stateA = int(random(0, 50));
    int stateB = int(random(0, 100));
    int stateC = int(random(20, 50));
    int stateD = int(random(0, 50));
    int stateE = int(random(0, 50));
    int stateF = int(random(0, 100));
    int stateG = int(random(20, 50));
    int stateH = int(random(0, 50));
    int stateI = int(random(0, 50));
    int Moisture = int(random(10, 150));
    String topic = "monitor/" + String(auth);
   magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"Switch1_10A\",\"Data\":\"" +  String(stateA)  + "\"},{\"Name\":\"Switch2_10A\",\"Data\":\"" +  String(stateB)  + "\"},{\"Name\":\"Switch3_10A\",\"Data\":\"" +  String(stateC)  + "\"},{\"Name\":\"Switch4_40A\",\"Data\":\"" +  String(stateD)  + "\"},{\"Name\":\"Moisture\",\"Data\":\"" +  String(Moisture)  + "\"},{\"Name\":\"Switch5_10A\",\"Data\":\"" +  String(stateE)  + "\"},{\"Name\":\"Switch6_10A\",\"Data\":\"" +  String(stateF)  + "\"},{\"Name\":\"Switch7_10A\",\"Data\":\"" +  String(stateG)  + "\"},{\"Name\":\"Switch8_10A\",\"Data\":\"" +  String(stateH)  + "\"},{\"Name\":\"Switch9_10A\",\"Data\":\"" +  String(stateI)  + "\"},{\"Name\":\"LAMPMT1\",\"Data\":\"" + MT1 + "\"},{\"Name\":\"LAMPMT2\",\"Data\":\"" + MT2 + "\"},{\"Name\":\"LAMPMT3\",\"Data\":\"" + MT3 + "\"},{\"Name\":\"LAMPMT4\",\"Data\":\"" + MT4 + "\"},{\"Name\":\"LAMPMT5\",\"Data\":\"" + MT5 + "\"}]}");
  }
  

  /*########################## Pub only 30 sec #############################*/
  if (millis() - lastMillis_2 > 30000) {
    lastMillis_2 = millis();
    int stateA = int(random(0, 50));
    int stateB = int(random(0, 100));
    int stateC = int(random(20, 50));
    int stateD = int(random(0, 50));
    int stateE = int(random(0, 50));
    int stateF = int(random(0, 100));
    int stateG = int(random(20, 50));
    int stateH = int(random(0, 50));
    int stateI = int(random(0, 50));
    int Moisture = int(random(10, 150));
    String topic = "monitor/" + String(auth);
    magel.publish(topic, "{\"DeviceId\":\"" + String(auth) + "\",\"Sensors\":[{\"Name\":\"Switch1_10A\",\"Data\":\"" +  String(stateA)  + "\"},{\"Name\":\"Switch2_10A\",\"Data\":\"" +  String(stateB)  + "\"},{\"Name\":\"Switch3_10A\",\"Data\":\"" +  String(stateC)  + "\"},{\"Name\":\"Switch4_40A\",\"Data\":\"" +  String(stateD)  + "\"},{\"Name\":\"Moisture\",\"Data\":\"" +  String(Moisture)  + "\"},{\"Name\":\"Switch5_10A\",\"Data\":\"" +  String(stateE)  + "\"},{\"Name\":\"Switch6_10A\",\"Data\":\"" +  String(stateF)  + "\"},{\"Name\":\"Switch7_10A\",\"Data\":\"" +  String(stateG)  + "\"},{\"Name\":\"Switch8_10A\",\"Data\":\"" +  String(stateH)  + "\"},{\"Name\":\"Switch9_10A\",\"Data\":\"" +  String(stateI)  + "\"},{\"Name\":\"LAMPMT1\",\"Data\":\"" + MT1 + "\"},{\"Name\":\"LAMPMT2\",\"Data\":\"" + MT2 + "\"},{\"Name\":\"LAMPMT3\",\"Data\":\"" + MT3 + "\"},{\"Name\":\"LAMPMT4\",\"Data\":\"" + MT4 + "\"},{\"Name\":\"LAMPMT5\",\"Data\":\"" + MT5 + "\"}]}");
  }

  if (subscrb == false)
  {
    delay(5000);
    Subscribe_list();
    delay(5000);
  }
}
