#include <MQTT.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
//#include <ArduinoJson.h>

////Library OLED////
#include <Wire.h> 
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

Adafruit_SSD1306 OLED(-1); 
////////////////////

////Library TempSensor/////
#include <OneWire.h>
#include <DallasTemperature.h>

////// Ssid&Pass  AP Mode //////
#define ESP_AP_NAME "SET_ME_SSID_ConsoleAPP"
#define ESP_AP_PASS "888888"

////// PIN //////
#define ONE_WIRE_BUS D5
#define LED D6
//#define LED_MQ_Connect D3
#define BUTTON D7  /// ปุ่ม RESET กดค้างแล้วกดรีเซ็ตจากนั้นปล่อย จะเข้าโหมด RESET SSID
//#define DHTTYPE DHT22


OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
////// Key Token //////

 const char* keyToken = "692b84b0-ac58-11e9-9aef-8de86578e580";

 const char* hostserverIP = "13.67.108.80";   //13.67.108.80 ifarm
 int port = 1883;
 
////// MQTT Setting ////
char clientID[] ="consoleapp";
char username[] ="admin";
char password[] ="admin";
//char subscribeTopic[] ="delta/get/fcb97d3a-c1fa-4cd6-84c9-c5df6b0d39de/";
//char publishTopic[] = "report/update/non/v1/fcb97d3a-c1fa-4cd6-84c9-c5df6b0d39de";

char publishTopic[] = "event/iot_service"; //Azure Service
  ////// Status ////

  ////// a=topic , b=payload //////
 String TP,PL;
  
   ////// Send time delay //////
 unsigned long lastMillis = 0;
 unsigned long lastMillis2 = 0;
 unsigned long lastMillisSW1 = 0;
 unsigned long lastMillisSW2 = 0;
 unsigned long lastMillisSW3 = 0;
 unsigned long lastMillisSW4 = 0;
 
  ////// Timer //////
 int TimeoutConnect = 120;
 
  ////// Timer //////
 int Timer1=0;
 int Sw1_10A=0;
 boolean condition_1=false;

 int connection =0;
 String t;
 float T_oled;
 float prev;
//DHT dht(DHTPIN, DHTTYPE);
MQTTClient client(1024);
WiFiClient net;
WiFiManager wifiManager;
StaticJsonDocument<200> doc;

/*--------------------------- Connect -------------------------------*/
void tick() 
  {
    digitalWrite(LED, HIGH);
    delay(50);
    digitalWrite(LED, LOW);
    delay(50);
  }

void connect() {
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("Offline ");
    ESP.reset();
    delay(10);
  }
  Serial.println("\nconnecting...");
  while (!client.connect(clientID, username, password)) 
  {
//    tick();
    Serial.print(".");
    delay(1000);
    
  }
   Serial.println("Connected");
   digitalWrite(LED,LOW);
//    client.subscribe(subscribeTopic); 
//   client.subscribe("dashboards/data/" + String(keyToken));
//   Serial.println("Subscribe Topic Success");
   connection =1;
//   digitalWrite(LED_MQ_Connect,HIGH);
}

/*--------------------------- messageReceived -------------------------------*/
void messageReceived(String &topic, String &payload ) 
{
  Serial.println("message : " + topic + " - " + payload );
  TP = topic;
  PL = payload;

    /*####################### Condition ON #############################*/
  if(TP =="dashboards/data/" + String(keyToken))
  {
  DeserializationError error = deserializeJson(doc, PL);

  if (error) 
      {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;
      }

    // int Switch1_10A = doc["Switch1_10A"];
    // int Timer = doc["Timer"];

   /*############################### Switch 1 #############################################*/
  // if(a == "dashboards/data/" + String(keyToken) && b== "{\"Switch1_10A\":1}" ){
  //   digitalWrite(RELAY_D1, HIGH);
  //   stateA=1;
  // //  client.publish("monitor/" + String(keyToken), " {\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Switch1_10A\",\"Data\":\"" +  String(stateA)  + "\"},{\"Name\":\"BUTTON_2\",\"Data\":\"" +  String(stateB)  + "\"},{\"Name\":\"BUTTON_3\",\"Data\":\"" +  String(stateC)  + "\"},{\"Name\":\"BUTTON_4\",\"Data\":\"" +  String(stateD)  + "\"}]} ");
  //   Serial.println("SWITCH1 : OPEN");
  //   delay(1);
  //   a="";
  //   b="";
  // }
  // if(a == "dashboards/data/" + String(keyToken) && b== "{\"Switch1_10A\":0}" ){
  //   digitalWrite(RELAY_D1, LOW);
  //   stateA=0;
  // //  client.publish("monitor/" + String(keyToken), " {\"DeviceId\":\"" + String(keyToken) + "\",\"Sensors\":[{\"Name\":\"Switch1_10A\",\"Data\":\"" +  String(stateA)  + "\"},{\"Name\":\"BUTTON_2\",\"Data\":\"" +  String(stateB)  + "\"},{\"Name\":\"BUTTON_3\",\"Data\":\"" +  String(stateC)  + "\"},{\"Name\":\"BUTTON_4\",\"Data\":\"" +  String(stateD)  + "\"}]} ");
  //   Serial.println("SWITCH1 : CLOSE");
  //   delay(1);
  //   a="";
  //   b="";
  // }

/*################################## Switch timer ################################################################*/  
  
// /*s1*/  if((Switch1_10A==1)&&(Timer>0)){
//     Timer1=Timer;
//     Timer1=Timer1*1000;
//     condition_1=true;
//     stateA=1;
//     digitalWrite(RELAY_D1, HIGH);
//     Serial.println("SWITCH1 : OPEN : TIMER1");
//     a = "";
//     b = "";

//      }
   }
}

/*--------------------------- LED AP -------------------------------*/


/*--------------------------- Setup -------------------------------*/

void oled_display()
{
   prev = T_oled;
//  Tint = t.toI
  T_oled = t.toFloat();
  
  if(prev > T_oled)
    {
      Serial.println("Down");
//      OLED.clearDisplay();
//      OLED.setTextSize(3);
      OLED.fillTriangle(5,20,11,27,17,20,WHITE);
//      OLED.drawTriangle(5,20,11,13,17,20,WHITE);
      OLED.display();
      delay(400);
    }
   if(prev < T_oled)
    {
      Serial.println("UP");
//      OLED.clearDisplay();
//      OLED.setTextSize(3);
      OLED.fillTriangle(5,20,11,13,17,20,WHITE);
      
//      OLED.drawTriangle(5,20,11,27,17,20,WHITE);
      OLED.display();
      delay(400);
    }
  OLED.clearDisplay();
    OLED.setTextColor(WHITE,BLACK); 
    OLED.setCursor(0,0);
    OLED.setTextSize(1); 
    OLED.println("Temperature : Celsius");
    OLED.setCursor(25,10);
    OLED.setTextSize(3);
    OLED.print(T_oled);
//    OLED.println("c");
    OLED.display();
   delay(400);
    

  Serial.print("Temp: ");
  Serial.println(T_oled);
}
void setup(void) {
  Serial.begin(115200);

 OLED.begin(SSD1306_SWITCHCAPVCC,0x3C);
 OLED.clearDisplay();
 OLED.setTextColor(WHITE,BLACK); 
 OLED.setCursor(0,0);
 OLED.setTextSize(2); 
 OLED.println("Temp SenSor");
 OLED.println("Ready");
 OLED.display();
 OLED.clearDisplay();
 delay(2000);
  
  Serial.println("MQTT Setup!! ");
  
//  while (!Serial) continue;

//  pinMode(DHTPIN, INPUT);
  pinMode(LED, OUTPUT);
//  pinMode(LED_MQ_Connect,OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);
  digitalWrite(LED,LOW);
//  digitalWrite(LED_MQ_Connect,LOW);
if(digitalRead(BUTTON) == LOW) // Press button
  {
    //reset saved settings
    wifiManager.resetSettings(); // go to ip 192.168.4.1 to config
    Serial.println("WiFi reset");
    
  }
  
//  ticker.attach(0.2, tick);

  wifiManager.setTimeout(TimeoutConnect);
  wifiManager.autoConnect(ESP_AP_NAME,ESP_AP_PASS);

  client.begin(hostserverIP, port, net);
  client.onMessage(messageReceived);
  connect();

  sensors.begin();
  // digitalWrite(RELAY_D1, LOW);
}

/*--------------------------- Loop -------------------------------*/
void loop(void) {
  
  /*############################### client LOOP ###################################*/
  client.loop();
  if(!client.connected()) 
  {
    Serial.println("Client MQTT Offline !!");
    connection =0;
//    digitalWrite(LED_MQ_Connect,LOW);
    tick();
    connect();
    
  }
  //DHT
//  Serial.print("Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperatures
//  Serial.println("DONE");
  delay(300);
  t =sensors.getTempCByIndex(0);
  //
  oled_display();
//  delay(300);
  if(connection == 1)
    {
      
      if (millis()-lastMillis > 2000)
      {
        lastMillis =millis();
        Serial.println("Temp : "+t);
//        Serial.println("Temp : "+String(t)+"  "+" Humi :"+String(h));
      }

         char payloadPublish[] ="";
    /*########################## Pub2 only 20 sec #############################*/   
      if (millis() - lastMillis2 > 5000) 
      {
      lastMillis2 = millis();
//      client.publish(publishTopic,"{\"Temp\":"+String(t)+",\"Hum\":"+String(h)+"}");   
//      client.publish("monitor/" + String(keyToken), " {\"Sensors\":[{\"Name\":\"Temp\",\"Data\":\"" +  String(t)  + "\"},{\"Name\":\"Humi\",\"Data\":\"" +  String(h)  + "\"}]} ");   
      client.publish(publishTopic, "{\"DeviceId\":\"f0e9d7e0-800c-11e8-ab5c-f5fb05055f9a\",\"Sensors\":[{\"Name\":\"Temp\",\"Data\":\""+String(t)+"\"},{\"Name\":\"Mag\",\"Data\":\"1\"}]}"); 
      Serial.println("Send Sensor");
      digitalWrite(LED,HIGH);
      delay(500);
      digitalWrite(LED,!HIGH);
      Serial.println("Publish Temp : "+String(t));
       }
    }
 
}
