#define G  5
#define R  6
#define B  7
#define com 8
void setup() {
pinMode(R, OUTPUT);
pinMode(G, OUTPUT);
pinMode(B, OUTPUT);
pinMode(com, OUTPUT);
Serial.begin(9600);
}
void loop() {
// กำหนด LED ดับ
Serial.println("White");
//analogWrite((R,G,B),0);
//analogWrite(G, 0);
//analogWrite(B, 0);
digitalWrite(com,HIGH);
digitalWrite(R, !HIGH);
digitalWrite(G, !HIGH);
digitalWrite(B, !HIGH);
delay(2000);
// กำหนด LED สีแดง
Serial.println("Red");
//analogWrite(R, 0);
//analogWrite(G, 255);
//analogWrite(B, 255);
digitalWrite(com,HIGH);
digitalWrite(R, !HIGH);
digitalWrite(G, HIGH);
digitalWrite(B, HIGH);
delay(2000);
// กำหนด LED สีเขียว
Serial.println("Green");
//analogWrite(R, 255);
//analogWrite(G, 0);
//analogWrite(B, 255);
digitalWrite(com,HIGH);
digitalWrite(R, HIGH);
digitalWrite(G, !HIGH);
digitalWrite(B, HIGH);
delay(2000);
// กำหนด LED สีน้ำเงิน
Serial.println("Blue");
//analogWrite(R, 255);
//analogWrite(G, 255);
//analogWrite(B, 0);

digitalWrite(com,HIGH);
digitalWrite(R, HIGH);
digitalWrite(G, HIGH);
digitalWrite(B, !HIGH);
delay(2000);
// กำหนด LED สีขาว
Serial.println("Off");
//analogWrite(R, 255);
//analogWrite(G, 255);
//analogWrite(B, 255);
digitalWrite(com,!HIGH);
digitalWrite(R, HIGH);
digitalWrite(G, HIGH);
digitalWrite(B, HIGH);
delay(2000);
}
