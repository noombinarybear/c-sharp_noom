int LED[] = { 12, 13};
void setup() {
  // put your setup code here, to run once:
pinMode(LED[0],OUTPUT);
pinMode(LED[1],OUTPUT);
}
void SetLED(int L1,int L2)
{
  digitalWrite(LED[0],L1);
  digitalWrite(LED[1],L2);
}
void loop() {
  SetLED(1,0);
  delay(500);
   SetLED(1,1);
  delay(500);
   SetLED(0,0);
  delay(500);
   SetLED(0,1);
  delay(500);
   SetLED(1,0);
  delay(500);
  // put your main code here, to run repeatedly:

}
