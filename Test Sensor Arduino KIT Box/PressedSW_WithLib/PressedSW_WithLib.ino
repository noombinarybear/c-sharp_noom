/*
  Name:		Pressed.ino
  Created:	9/5/2018 10:49:52 AM
  Author:	Evert Arias
  Description: Example to demostrate how to use the library to detect a single pressed on a button.
*/

#include <EasyButton.h>

// Arduino pin where the button is connected to.


#define MT1LED 53
#define MT2LED 52
#define MT3LED 51
#define MT4LED 50
#define MT5LED 49

#define SW1 33
#define SW2 32
#define SW3 31
#define SW4 30
#define SW5 28

// Instance of the button.
EasyButton button1(SW1);
EasyButton button2(SW2);
EasyButton button3(SW3);
EasyButton button4(SW4);
EasyButton button5(SW5);
int power1 = LOW;
int power2 = LOW;
int power3 = LOW;
int power4 = LOW;
int power5 = LOW;
//int state1 =0;
int status_button_1 = LOW;
int status_button_2 = LOW;
int status_button_3 = LOW;
int status_button_4 = LOW;
int status_button_5 = LOW;
// Callback function to be called when the button is pressed.
//void onPressed() {
//  Serial.println("Button has been pressed!");
//}

void onButton1Pressed()
{
  status_button_1 = HIGH;
  if (status_button_1 == HIGH)
  {
    Serial.println("Button1 has been pressed!");
    power1 = !power1;
    digitalWrite(MT1LED, power1 );
    status_button_1 = LOW;
  }
}
void onButton2Pressed()
{
  status_button_2 = HIGH;
  if (status_button_2 == HIGH)
  {
    Serial.println("Button1 has been pressed!");
    power2 = !power2;
    digitalWrite(MT2LED, power2 );
    status_button_2 = LOW;
  }
}
void onButton3Pressed()
{
  status_button_3 = HIGH;
  if (status_button_3 == HIGH)
  {
    Serial.println("Button1 has been pressed!");
    power3 = !power3;
    digitalWrite(MT3LED, power3 );
    status_button_3 = LOW;
  }
}
void onButton4Pressed()
{
  status_button_4 = HIGH;
  if (status_button_4 == HIGH)
  {
    Serial.println("Button1 has been pressed!");
    power4 = !power4;
    digitalWrite(MT4LED, power4 );
    status_button_4 = LOW;
  }
}
void onButton5Pressed()
{
  status_button_5 = HIGH;
  if (status_button_5 == HIGH)
  {
    Serial.println("Button1 has been pressed!");
    power5 = !power5;
    digitalWrite(MT5LED, power5 );
    status_button_5 = LOW;
  }
}


void setup() {
  // Initialize Serial for debuging purposes.
  Serial.begin(9600);
  // Initialize the button.
  button1.begin();
  button2.begin();
  button3.begin();
  button4.begin();
  button5.begin();
  button1.onPressed(onButton1Pressed);
  button2.onPressed(onButton2Pressed);
  button3.onPressed(onButton3Pressed);
  button4.onPressed(onButton4Pressed);
  button5.onPressed(onButton5Pressed);
  // Add the callback function to be called when the button is pressed.
  //  button.onPressed(onPressed);
  pinMode(MT1LED, OUTPUT);
  digitalWrite(MT1LED, LOW);
  pinMode(MT2LED, OUTPUT);
  digitalWrite(MT2LED, LOW);
  pinMode(MT3LED, OUTPUT);
  digitalWrite(MT3LED, LOW);
  pinMode(MT4LED, OUTPUT);
  digitalWrite(MT4LED, LOW);
  pinMode(MT5LED, OUTPUT);
  digitalWrite(MT5LED, LOW);
}

void loop() {
  // Continuously read the status of the button.
  button1.read();
  button2.read();
  button3.read();
  button4.read();
  button5.read();
}
