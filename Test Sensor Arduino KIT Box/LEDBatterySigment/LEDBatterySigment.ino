//        8    7   6   5
//        ||  ||  ||  ||                     common+   B  C  D  E  F       LED Alround Battery 8+  A =1
//    |||||||||||||||||||||||| A              7        2  3  4  5  6
//    || //  //  //  //  //  ||||
//    || B   C   D   E   F   ||||
//    || \\  \\  \\  \\  \\  ||||
//    ||||||||||||||||||||||||
//        ||  ||  ||  ||
//        1    2   3   4
//
#define ledB  2
#define ledC  3
#define ledD  4
#define ledE  5
#define ledF  6
#define ledA  7

#define COMMON_ANODE

void setup() {
  pinMode(ledB, OUTPUT);
  pinMode(ledC, OUTPUT);
  pinMode(ledD, OUTPUT);
  pinMode(ledE, OUTPUT);
  pinMode(ledF, OUTPUT);
  pinMode(ledA, OUTPUT);
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  setValue(255, 255, 255, 255, 255, 255); // 100%
  delay(500);
  setValue(255, 255, 255, 255, 255, 0); // 80%
  delay(500);
  setValue(255, 255, 255, 255, 0, 0); // 60%
  delay(500);
  setValue(255, 255, 255, 0, 0, 0); // 40%
  delay(500);
  setValue(255, 255, 0, 0, 0, 0); // 20%
  delay(500);
  setValue(255, 0, 0, 0, 0, 0); // 0%
  delay(500);
  setValue(0, 0, 0, 0, 0, 0); // 0%
  delay(500);
}
/////              A          B          C          D         E         F
void setValue(int posA, int pos20, int pos40, int pos60, int pos80, int pos100)
{
#ifdef COMMON_ANODE
  posA = 255 - posA;
  pos20 = 255 - pos20;
  pos40 = 255 - pos40;
  pos60 = 255 - pos60;
  pos80 = 255 - pos80;
  pos100 = 255 - pos100;
#endif
  analogWrite(ledA, posA);
  analogWrite(ledB, pos20);
  analogWrite(ledC, pos40);
  analogWrite(ledD, pos60);
  analogWrite(ledE, pos80);
  analogWrite(ledF, pos100);
}
