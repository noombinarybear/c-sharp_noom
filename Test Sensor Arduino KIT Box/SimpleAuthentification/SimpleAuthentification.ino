#ifdef ESP8266
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
ESP8266WebServer server(80);
#else
#include "index.h"
#include "second.h"
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
WebServer server(80);
#endif

const char* ssid = "Orbi The Gentry";
const char* password = "Virus@coth";
String genV;
unsigned long premillis;
//Check if header is present and correct
bool is_authentified() {
  Serial.println("Enter is_authentified");
  if (server.hasHeader("Cookie")) {
    Serial.print("Found cookie: ");
    String cookie = server.header("Cookie");
    Serial.println(cookie);
    if (cookie.indexOf("ESPSESSIONID=1") != -1) {
      Serial.println("Authentification Successful");
      return true;
    }
  }
  Serial.println("Authentification Failed");
  return false;
}

//login page, also called for disconnect
void handleLogin() {
  String msg;
  if (server.hasHeader("Cookie")) {
    Serial.print("Found cookie: ");
    String cookie = server.header("Cookie");
    Serial.println(cookie);
  }
  if (server.hasArg("DISCONNECT")) {
    Serial.println("Disconnection");
    server.sendHeader("Location", "/login");
    server.sendHeader("Cache-Control", "no-cache");
    server.sendHeader("Set-Cookie", "ESPSESSIONID=0");
    server.send(301);
    return;
  }
  if (server.hasArg("USERNAME") && server.hasArg("PASSWORD")) {
    if (server.arg("USERNAME") == "admin" &&  server.arg("PASSWORD") == "admin" ) {
      server.sendHeader("Location", "/");
      server.sendHeader("Cache-Control", "no-cache");
      server.sendHeader("Set-Cookie", "ESPSESSIONID=1");
      server.send(301);
      Serial.println("Log in Successful");
      return;
    }
    msg = "Wrong username/password! try again.";
    Serial.println("Log in Failed");
  }
  String content = "<html><body><form action='/login' method='POST'>To log in, please use : admin/admin<br>";
  content += "User:<input type='text' name='USERNAME' placeholder='user name'><br>";
  content += "Password:<input type='password' name='PASSWORD' placeholder='password'><br>";
  content += "<input type='submit' name='SUBMIT' value='Submit'></form>" + msg + "<br>";
  content += "You also can go <a href='/inline'>here</a></body></html>";
  server.send(200, "text/html", content);
}

//root page can be accessed only if authentification is ok
void handleDash() {
  String s = MAIN_page; //Read HTML contents
  server.send(200, "text/html", s); //Send web page
}
void handleDash_2() {
  String d2 = MAIN_second; //Read HTML contents
  server.send(200, "text/html", d2); //Send web page
}

void handleRoot() {
  Serial.println("Enter handleRoot");
  String header;
  if (!is_authentified()) {
    server.sendHeader("Location", "/login");
    server.sendHeader("Cache-Control", "no-cache");
    server.send(301);
    return;
  }
  String content = "<html><body><H2>hello, you successfully connected to esp8266/esp32!</H2><br>";
  if (server.hasHeader("User-Agent")) {
    content += "the user agent used is : " + server.header("User-Agent") + "<br><br>";
  }
  //  Serial.println(String(WiFi.localIP()));
  String ip = String(WiFi.localIP());
  content += "You can access this page until you <a href=\"/login?DISCONNECT=YES\">disconnect</a></body></html>";
  content += "You can access this page to <a href=\"/dashboard\">Dashboard</a></body></html>";
  content += "You can access this page to <a href=\"/dashboard2\">Dashboard2</a></body></html>";
  server.send(200, "text/html", content);


}

//no need authentification
void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void) {
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

#ifdef ESP8266
  if (MDNS.begin("little8266")) {
#else
  if (MDNS.begin("little32")) {
#endif
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);
  server.on("/login", handleLogin);
  server.on("/count", gennerateValue);
  server.on("/readADC", handleADC);
  server.on("/readADC2", handleADC2);
  server.on("/dashboard", handleDash);
  server.on("/dashboard2", handleDash_2);
  server.on("/inline", []()

  {
    server.send(200, "text/plain", "this works without need of authentification");
  });

  server.onNotFound(handleNotFound);
  //here the list of headers to be recorded
  const char * headerkeys[] = {"User-Agent", "Cookie"} ;
  size_t headerkeyssize = sizeof(headerkeys) / sizeof(char*);
  //ask server to track these headers
  server.collectHeaders(headerkeys, headerkeyssize );
  server.begin();
  Serial.println("HTTP server started");
}

void handleADC()
{
  String adcValue = String(random(0, 100));
  server.send(200, "text/plane", adcValue); //Send ADC value only to client ajax request
  //  String s = MAIN_page; //Read HTML contents
  //  server.send(200, "text/html", s); //Send web page
}
void handleADC2()
{
  String ADCValuePart2 = String(random(20, 150));
  server.send(200, "text/plane", ADCValuePart2); //Send ADC value only to client ajax request
  //  String s = MAIN_page; //Read HTML contents
  //  server.send(200, "text/html", s); //Send web page
}

void gennerateValue()
{
  if (millis() - premillis > 5000)
  {
    premillis = millis();
    genV = String(random(0, 100));
  }
  String block = "<html><head><style>.cities { background-color: black; color: white; margin: 20px; padding: 20px;} ";
  block += "</style> </head><body><div class=\"cities\"><h2>" + genV + "</h2><p>Gennerate Value Block.</p></div></body></html>";
  server.send(200, "text/html", block);
}
void loop(void) {
  server.handleClient();
  delay(1);
}
