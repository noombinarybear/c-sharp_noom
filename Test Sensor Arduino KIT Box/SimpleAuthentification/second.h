const char MAIN_second[] PROGMEM = R"=====(
<!DOCTYPE html>
<html>
<style>
.card{
    max-width: 400px;
     min-height: 250px;
     background: #02b875;
     padding: 30px;
     box-sizing: border-box;
     color: #FFF;
     margin:20px;
     box-shadow: 0px 2px 18px -4px rgba(0,0,0,0.75);
}
</style>
<body>
 
<div class="card">
  <h4>The ESP32 Dashboard Past2</h4><br>
  <h1>Sensor2 Value:<span id="ADCValuePart2">0</span></h1><br>
  <h4>You can access this page to <a href=/>Main</a></span></h4><br>
</div>
<script>
 
setInterval(function() {
  // Call a function repetatively with 2 Second interval
  getData();
}, 2000); //2000mSeconds update rate
 
function getData() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("ADCValuePart2").innerHTML =
      this.responseText;
    }
  };
  xhttp.open("GET", "readADC2", true);
  xhttp.send();
}
</script>
</body>
</html>
)=====";
